<?php

namespace Tests\Unit\Application\UseCases\Admins\Category;

use App\Application\UseCases\Admins\Category\CreateCategoryCase;
use App\Domain\Category\Category;
use App\Domain\Category\Contracts\CategoryService;
use App\Presentation\Requests\CreateCategory;
use Tests\TestCase;
use Mockery;

class CreateCategoryCaseTest extends TestCase
{
    /**
     * @var CategoryService|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private CategoryService $categoryService;

    /**
     * @var CreateCategory|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private CreateCategory $request;

    /**
     * @var CreateCategoryCase
     */
    private CreateCategoryCase $case;

    public function setUp(): void
    {
        $this->categoryService = Mockery::mock(CategoryService::class);
        $this->case = new CreateCategoryCase($this->categoryService);
        $this->request = Mockery::mock(CreateCategory::class);
    }

    public function testCreateCategoryIsCalled()
    {
        $this->request->allows('get')->with('name')->andReturn('test');
        $this->categoryService->shouldReceive('createCategory')->with('test')->andReturn(Category::class)->once();

    }
}
