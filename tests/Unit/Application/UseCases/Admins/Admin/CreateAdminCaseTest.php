<?php

namespace Tests\Unit\Application\UseCases\Admins\Admin;

use App\Application\UseCases\Admins\Admin\CreateAdminCase;
use App\Domain\User\Contracts\UserService;
use App\Domain\User\User;
use Tests\TestCase;
use Mockery;

final class CreateAdminCaseTest extends TestCase
{
    /**
     * @var User|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private User $user;

    /**
     * @var UserService|Mockery\LegacyMockInterface|Mockery\MockInterface
     */
    private UserService $userService;

    /**
     * @var CreateAdminCase
     */
    private CreateAdminCase $createAdminCase;

    public function setUp(): void
    {
        $this->user = Mockery::mock(User::class);
        $this->userService = Mockery::mock(UserService::class);
        $this->createAdminCase = new CreateAdminCase($this->userService);
    }

    public function testCreateAdminIsCalled()
    {
        $array = [];
        $this->userService->shouldReceive('createAdmin')->once();
        $this->createAdminCase->createAdmin($array);
    }

    public function testCreateAdminIsReturnUser()
    {
        $array = [];
        $this->userService->shouldReceive('createAdmin')->andReturn($this->user)->once();
        $this->assertEquals($this->createAdminCase->createAdmin($array), $this->user);
    }
}
