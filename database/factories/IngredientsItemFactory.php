<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain\IngredientsItem\IngredientItem;
use Faker\Generator as Faker;

$factory->define(
    IngredientItem::class,
    function (Faker $faker) {
        return [
            IngredientItem::NAME => $faker->name
        ];
    }
);
