<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain\Dish\Dish;
use App\Domain\Images\Images;
use App\Domain\User\User;
use Faker\Generator as Faker;

$factory->define(Images::class, function (Faker $faker) {
    return [
        Images::PATH => 'images/admin.jpg',
        Images::IMAGEABLE_ID => 1,
        Images::IMAGEABLE_TYPE => User::class
    ];
});
