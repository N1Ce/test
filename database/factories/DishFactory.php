<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain\Dish\Dish;
use Faker\Generator as Faker;

$factory->define(
    Dish::class,
    function (Faker $faker) {
        return [
            Dish::CATEGORY_ID => rand(1, 5),
            Dish::NAME => $faker->name,
            Dish::DESCRIPTION => $faker->text,
            Dish::PRICE => $faker->randomFloat(2, 10, 100),
            Dish::WEIGHT => rand(30, 200),
        ];
    }
);
