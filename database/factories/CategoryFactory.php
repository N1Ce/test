<?php
/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domain\Category\Category;
use Faker\Generator as Faker;

$factory->define(
    Category::class,
    function (Faker $faker) {
        return [
            Category::NAME => $faker->name
        ];
    }
);
