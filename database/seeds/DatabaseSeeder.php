<?php

use App\Domain\Category\Category;
use App\Domain\Dish\Dish;
use App\Domain\Images\Images;
use App\Domain\IngredientsItem\IngredientItem;
use App\Domain\User\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class, 5)->create();
        factory(Dish::class, 500)->create();
        factory(Images::class, 500)->create();
        factory(User::class, 1)->create();
        factory(IngredientItem::class, 10)->create();
    }
}
