<?php

use Illuminate\Support\Facades\Route;

Route::get('basket/', 'Clients\Basket\ShowBasketController');
Route::get('addBasket/{id}', 'Clients\Basket\AddBasketController');
Route::get('delete/{id}', 'Clients\Basket\RemoveBasketController');
Route::get('remove_items/{id}', 'Clients\Basket\RemoveBasketItemsController');
