<?php

use Illuminate\Support\Facades\Route;

Route::post('delete_ingredient', 'Admin\Ingredients\Delete\DeleteIngredientController')
    ->prefix('admin')
    ->middleware('auth');
