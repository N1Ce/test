<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('update_ingredient/{id}', 'Admin\Ingredients\Update\ShowUpdateIngredientController');
    Route::post('update_ingredient', 'Admin\Ingredients\Update\UpdateIngredientController');
});
