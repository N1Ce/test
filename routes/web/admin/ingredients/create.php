<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware('auth')->group(function() {
    Route::get('create_ingredient', 'Admin\Ingredients\Create\ShowCreateIngredientController');
    Route::post('create_ingredient', 'Admin\Ingredients\Create\CreateIngredientController');
});

