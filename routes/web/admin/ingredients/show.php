<?php

use Illuminate\Support\Facades\Route;

Route::get('ingredients', 'Admin\Ingredients\Show\ShowIngredientsController')
    ->prefix('admin')
    ->middleware('auth');
