<?php

use Illuminate\Support\Facades\Route;

Route::get('dashboard', 'Admin\Dashboard\DashboardController')->prefix('admin')->middleware('auth');
