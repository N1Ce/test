<?php

use Illuminate\Support\Facades\Route;

Route::get('dishes', 'Admin\Dish\Show\ShowDishController')
    ->prefix('admin')
    ->middleware('auth');
