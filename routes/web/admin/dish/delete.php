<?php

use Illuminate\Support\Facades\Route;

Route::post('delete_dish', 'Admin\Dish\Delete\DeleteDishController')
    ->prefix('admin')
    ->middleware('auth');
