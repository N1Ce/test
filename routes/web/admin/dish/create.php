<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('create_dish', 'Admin\Dish\Create\ShowCreateDishController');
    Route::post('create_dish', 'Admin\Dish\Create\CreateDishController');
});
