<?php

use Illuminate\Support\Facades\Route;

Route::get('update_dish/{id}', 'Admin\Dish\Update\ShowUpdateDishController')
    ->prefix('admin')
    ->middleware('auth');
