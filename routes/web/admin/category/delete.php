<?php

use Illuminate\Support\Facades\Route;

Route::post('delete_category', 'Admin\Category\Delete\DeleteCategoryController')
    ->prefix('admin')
    ->middleware('auth');

