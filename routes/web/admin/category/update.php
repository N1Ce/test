<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('update_category/{id}', 'Admin\Category\Update\ShowUpdateCategoryController');
    Route::post('update_category', 'Admin\Category\Update\UpdateCategoryController');
});
