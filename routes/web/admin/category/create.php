<?php

use Illuminate\Support\Facades\Route;

Route::post('create_category', 'Admin\Category\Create\CreateCategoryController')
    ->prefix('admin')
    ->middleware('auth');
