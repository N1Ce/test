<?php

use Illuminate\Support\Facades\Route;

Route::get('create_category', 'Admin\Category\Create\ShowCreateCategoryController')
    ->prefix('admin')
    ->middleware('auth');

Route::get('category', 'Admin\Category\Show\ShowCategoryController')
    ->prefix('admin')
    ->middleware('auth');
