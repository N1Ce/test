<nav class="pcoded-navbar">
    <div class="nav-list">
        <div class="pcoded-inner-navbar main-menu">
            <ul class="pcoded-item pcoded-left-item">
                <li>
                    <a href="{{ url('/') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-home"></i>
                        </span>
                        <span class="pcoded-mtext">Shop</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}">
                    <a href="{{ url('admin/dashboard') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-dashboard"></i>
                        </span>
                        <span class="pcoded-mtext">Dashboard</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/category') ? 'active' : '' }}">
                    <a href="{{ url('admin/category') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="fa fa-list-alt" aria-hidden="true"></i></span>
                        <span class="pcoded-mtext">Category</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/ingredients') ? 'active' : '' }}">
                    <a href="{{ url('admin/ingredients') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-tags" aria-hidden="true"></i>
                        </span>
                        <span class="pcoded-mtext">Ingredients</span>
                    </a>
                </li>
                <li class="{{ Request::is('admin/dishes') ? 'active' : '' }}">
                    <a href="{{ url('admin/dishes') }}" class="waves-effect waves-dark">
                        <span class="pcoded-micon">
                            <i class="fa fa-cutlery" aria-hidden="true"></i>
                        </span>
                        <span class="pcoded-mtext">Dishes</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
