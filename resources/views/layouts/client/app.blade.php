<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.client.includes.head')
</head>
<body style="background-color: #efefef">
<div id="app">
    @include('layouts.client.includes.top')
    @include('layouts.client.includes.header')
    @includeWhen(auth()->check(), 'web.admin.panel')
    <div class="container" style="margin-top:30px">
        @yield('content')
    </div>

    <div class="jumbotron text-center" style="margin-bottom:0">
        <p>Footer</p>
    </div>
</div>
<script type="text/javascript" src="/js/app.js"></script>
@yield('scripts')
</body>
</html>
