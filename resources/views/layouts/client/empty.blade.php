<!doctype html>
<html lang="en" class="h-100">
<head>
    @include('layouts.client.includes.head')
</head>
<body id="app" class="h-100">
<div class="container">
    @yield('content')
</div>

</body>
</html>
