<div class="dropdown">
    <button class="btn btn-success btn-sm ml-3" data-toggle="dropdown">
        <i class="fa fa-shopping-cart"></i> Basket
        <span class="badge badge-light">
            @if(isset($basket))
                {{$basket->getTotalQty()}}
            @else
                0
            @endif
        </span>
    </button>

    <div class="dropdown-menu" style="width: max-content">
            @if($basket and !empty($basket->getItems()))
            <table class="table table-hover mb-0">
                <thead class="thead-light">
                <tr>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($basket->getItems() as $item)
                    <tr>
                        <td>{{ $item->getProductName() }}</td>
                        <td>{{ sprintf("%.2f", $item->getPrice()) }}</td>
                        <td>
                            <a class="btn btn-sm btn-danger" href="{{ url('delete/'.$item->getProductID()) }}"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                @endforeach
            <tr>
                <td colspan="4">
                    <div class="d-flex justify-content-around">
                        Total sum:
                        <span class="text-right">{{ sprintf("%.2f", $basket->calculateSum()) }}</span>
                    </div>
                </td>
            </tr>
                <tr>
                    <td colspan="4" class="text-center">
                        <a href="{{ url('basket/') }}" class="btn btn-success">view basket</a>
                    </td>
                </tr>
            </tbody>
            @else
                <div class="dropdown-item">Yor basket is empty</div>
            @endif

        </table>
    </div>
</div>
