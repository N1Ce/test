@if ($errors->any())
    <div class="alert alert-danger alert-dismissible mt-2">
        <ul class="msg-list">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
