<div class="list-group">
    @foreach($categories as $category)
        <a href="{{ url('/category/'.$category->getID()) }}"
           class="list-group-item list-group-item-action {{ request()->is('category/'.$category->getID()) ? 'active' : '' }}">
                {{$category->getName()}}
        </a>
    @endforeach
</div>