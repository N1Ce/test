@extends('layouts.admin.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Ingredients</h5>
                    <a href="{{ url('admin/create_ingredient') }}"
                       class="btn btn-success pull-right mr-5">Create</a>
                </div>
                <div class="card-block">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Input</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ingredients as $ingredient)
                            <tr>
                                <td>{{ $ingredient->getName() }}</td>
                                <td class="d-flex">
                                    <a href="{{ url('admin/update_ingredient/'. $ingredient->getKey() )}}"
                                       class="btn btn-primary btn-sm">Update</a>
                                    <form action="{{ url('admin/delete_ingredient') }}" method="POST">
                                        @csrf
                                        <input type="number" class="d-none" value="{{ $ingredient->getKey() }}" name="id">
                                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="12">
                                <div class="card-footer d-flex justify-content-center">
                                    {{ $ingredients->links() }}
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
