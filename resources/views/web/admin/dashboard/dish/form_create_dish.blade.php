@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="card col-lg-6 mt-4" style="margin: 0 auto">
            <h1 class="card-header text-center">Create dish</h1>
            <div class="card-body">
                <form method="POST" action="{{ url('admin/create_dish') }}" enctype="multipart/form-data">
                    @csrf
                    {{-- Name --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="name">Name</label>
                        </div>
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                               name="name"
                               required>

                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- Category --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="category">Category: </label>
                        </div>
                        <select id="category" data-placeholder="Add category"
                                class="form-control @error('category') is-invalid @enderror" name="category">
                            @foreach($response->getCategories() as $category)
                                <option value="{{ $category->getKey() }}">{{ $category->getName() }}</option>
                            @endforeach
                        </select>
                        @error('category')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- Price --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="price">Price: </label>
                        </div>
                        <input id="price" type="number" step="0.01" min="0"
                               class="form-control @error('price') is-invalid @enderror" placeholder="price"
                               aria-label="Price" name="price">
                        @error('price')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- Weight --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="weight">Weight: </label>
                        </div>
                        <input id="weight" type="number" step="1" min="0"
                               class="form-control @error('weight') is-invalid @enderror" placeholder="weight"
                               aria-label="weight" name="weight">
                        <div class="input-group-append">
                            <span class="input-group-text">grams</span>
                        </div>
                        @error('weight')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- Ingredients --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="ingredients">Ingredients: </label>
                        </div>
                        <select id="ingredients" multiple data-placeholder="Add ingredients"
                                class="@error('ingredients') is-invalid @enderror" name="ingredients[]">
                            @foreach($response->getIngredients() as $ingredient)
                                <option value="{{ $ingredient->getKey() }}">{{ $ingredient->getName() }}</option>
                            @endforeach
                        </select>
                        @error('ingredients')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- File --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">Upload: </span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input @error('file') is-invalid @enderror" id="file" name="file">
                            <label class="custom-file-label" for="file" >Choose file</label>
                        </div>
                        @error('file')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- Description --}}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <label class="input-group-text" for="description">Description: </label>
                        </div>
                        <textarea name="description" class="form-control @error('description') is-invalid @enderror"
                                  id="description" rows="1"></textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {{-- submit --}}
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
