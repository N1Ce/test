@extends('layouts.admin.app')

@section('content')
    <div class="card">
        <div class="card-header">
            <h5>Dishes</h5>
            <a href="{{ url('admin/create_dish') }}"
               class="btn btn-success pull-right mr-5">Create</a>
        </div>
        <div class="card-block">
            <table id="demo-foo-filtering" class="table table-striped footable breakpoint-lg">
                <thead>
                <tr>
                    <th class="footable-sortable footable-first-visible" style="display: table-cell; border: none; cursor: pointer">
                        Image
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    </th>
                    <th data-breakpoints="xs" class="footable-sortable" style="display: table-cell; border: none; cursor: pointer">
                        Name
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    </th>
                    <th data-breakpoints="xs" class="footable-sortable" style="display: table-cell; border: none; cursor: pointer">
                        Category
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    </th>
                    <th data-breakpoints="xs" class="footable-sortable" style="display: table-cell; border: none; cursor: pointer">
                        Price
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    </th>
                    <th data-breakpoints="xs" class="footable-sortable" style="display: table-cell; border: none; cursor: pointer">
                        Weight
                        <i class="fa fa-arrows-v" aria-hidden="true"></i>
                    </th>
                    <th class="footable-sortable footable-last-visible" style="display: table-cell; border: none">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach($dishes as $dish)
                    <tr>
                        <td class="footable-first-visible" style="display: table-cell;">
                            <div style="height: 100px; width: 100px; background-image: url({{ empty($dish->getImagePath()) ? '/images/2.png' : '/'.$dish->getImagePath() }}); background-size: cover; background-position: center"></div>
                        </td>
                        <td style="display: table-cell;">{{ $dish->getName() }}</td>
                        <td style="display: table-cell;">{{ $dish->getCategoryName() }}</td>
                        <td style="display: table-cell;">{{ $dish->getPrice() }}</td>
                        <td style="display: table-cell;">{{ $dish->getWeight() }}</td>
                        <td class="footable-last-visible">
                            <a href="{{ url('admin/update_dish/' . $dish->getID()) }}"
                               class="btn btn-primary btn-sm">Update</a>
                            <form action="{{ url('admin/delete_dish') }}" method="POST">
                                @csrf
                                <input type="number" class="d-none" value="{{ $dish->getID() }}" name="id">
                                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="12">
                        <div class="card-footer d-flex justify-content-center">
                            {{ $dishes->links() }}
                        </div>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
