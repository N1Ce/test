@extends('layouts.admin.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Categories</h5>
                    <a href="{{ url('admin/create_category') }}"
                       class="btn btn-success pull-right mr-5">Create</a>
                </div>
                <div class="card-block">
                    <table class="table">
                        <thead class="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Slag</th>
                            <th>Input</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->getName() }}</td>
                                <td>-</td>
                                <td class="d-flex">
                                    <a href="{{ url('admin/update_category/' . $category->getID()) }}"
                                       class="btn btn-primary btn-sm">Update</a>
                                    <form action="{{ url('admin/delete_category') }}" method="POST">
                                        @csrf
                                        <input type="number" class="d-none" value="{{ $category->getID() }}" name="id">
                                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
