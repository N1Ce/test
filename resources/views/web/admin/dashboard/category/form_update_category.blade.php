@extends('layouts.admin.app')

@section('content')
    <div class="container">
        <div class="card col-lg-6 mt-4" style="margin: 0 auto">
            <h1 class="card-header text-center">Update category</h1>

            <div class="card-body">
                <form method="POST" action="{{ url('admin/update_category') }}">
                    @csrf
                    <input type="number" value="{{ $category->getKey() }}" class="d-none" name="id">
                    <div class="form-group row">
                        <label for="category"
                               class="col-md-4 col-form-label text-md-right">Category name:</label>

                        <div class="col-md-6">
                            <input id="category" type="text"
                                   class="form-control @error('name') is-invalid @enderror" name="name"
                                   required value="{{ $category->getName() }}">

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="flag"
                               class="col-md-4 col-form-label text-md-right">Flag:</label>

                        <div class="col-md-6">
                            <input id="flag" type="text"
                                   class="form-control @error('flag') is-invalid @enderror" name="flag"
                                   readonly >

                            @error('flag')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Create
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
