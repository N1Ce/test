@extends('layouts.client.app')

@section('content')
    <div class="card shopping-cart">
        <div class="card-header bg-dark text-light">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
            Shipping cart
            <a href="{{ url('/') }}" class="btn btn-outline-info btn-sm pull-right">Continiu shopping</a>
            <div class="clearfix"></div>
        </div>
        <div class="card-body" id="card-body">
            @if($basket->isNotEmpty())
                @foreach($basket->groupBy('id') as $items)
                    <div class="row align-items-center element">
                        <div class="col-12 col-sm-12 col-md-2 text-center">
                            <img class="img-responsive"
                                 src="{{ empty($items->first()->getImagePath()) ? '/images/2.png' : $items->first()->getImagePath()}}"
                                 alt="prewiew"
                                 width="120" height="80">
                        </div>
                        <div class="col-12 text-sm-center col-sm-12 text-md-left col-md-6">
                            <h4 class="product-name"><strong>{{ $items->first()->getName() }}</strong></h4>
                            <h4>
                                <small>{{ $items->first()->getDescription() }}</small>
                            </h4>
                        </div>
                        <div class="col-12 col-sm-12 text-sm-center col-md-4 text-md-right row align-items-center item">
                            <div class="col-3 col-sm-3 col-md-6 text-md-right" style="padding-top: 5px">
                                <h6><strong class="price">{{
                                        sprintf("%.2f",
                                            $items->map(function ($value) {
                                                 return $value->getPrice();
                                            })->sum())
                                        }}</strong><span class="text-muted"> x</span></h6>
                            </div>
                            <div class="col-4 col-sm-4 col-md-4">
                                <div class="quantity">
                                    <input type="button" value="+" class="plus"
                                           onclick="add(this, {{$items->first()}})">
                                    <input type="number" step="1" max="99" min="1" value="{{ $items->count() }}"
                                           data-item-id="{{ $items->first()->id() }}" class="qty"
                                           size="4" disabled>
                                    <input type="button" value="-" class="minus"
                                           onclick="remove(this, {{$items->first()}})">
                                </div>
                            </div>
                            <div class="col-2 col-sm-2 col-md-2 text-right">
                                <button type="button" class="btn btn-outline-danger btn-xs"
                                        onclick="removeItems(this, {{$items->first()}})">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="card-footer">
            <div class="coupon col-md-5 col-sm-5 no-padding-left pull-left">
                <div class="row">
                    <div class="col-6">
                        <input type="text" class="form-control" placeholder="cupone code">
                    </div>
                    <div class="col-6">
                        <input type="submit" class="btn btn-default" value="Use cupone">
                    </div>
                </div>
            </div>
            <div class="pull-right" style="margin: 10px">
                <a href="" class="btn btn-success pull-right">Checkout</a>
                <div class="pull-right" style="margin: 5px">
                    Total price:
                    <b>
                        <span id="totalSum">
                            {{
                            sprintf("%.2f",
                                $basket->sum(function ($item) {
                                    return $item->getPrice();
                                })
                            )
                            }}
                        </span>
                        грн.
                    </b>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript">

        function add(element, item) {
            let price = $(element).closest('.item').find('.price');
            let qtyInput = $(element).siblings('.qty');
            let qty = qtyInput.val();
            let totalSum = $('#totalSum');
            qty++;

            $.ajax({
                url: "addBasket/" + item.id,
                success: function () {
                    price.text(parseFloat((item.price * qty).toFixed(2)).toFixed(2));
                    qtyInput.val(qty);
                    totalSum.text((Number(totalSum.text()) + Number(item.price)).toFixed(2));
                }
            });
        }

        function remove(element, item) {
            console.log();
            let price_sample = $('.qty["data-item-id" = item.id]')
            let price = $(element).closest('.item').find('.price');
            let qtyInput = $(element).siblings('.qty');
            let qty = qtyInput.val();
            let totalSum = $('#totalSum');
            qty--;
            if (qty < 1) {
                $(element).closest('.element').remove();
            }

            $.ajax({
                url: "delete/" + item.id,
                success: function (result) {
                    price.text(parseFloat((item.price * qty).toFixed(2)).toFixed(2));
                    qtyInput.val(qty);
                    totalSum.text((Number(totalSum.text()) - Number(item.price)).toFixed(2));
                }
            });
        }

        function removeItems(element, item) {
            let qtyInput = $(element).closest('.item').find('.qty');
            let qty = qtyInput.val();
            let totalSum = $('#totalSum');

            $.ajax({
                url: "remove_items/" + item.id,
                success: function () {
                    $(element).closest('.element').remove();
                    totalSum.text((Number(totalSum.text()) - Number(item.price * qty)).toFixed(2));
                }
            });
        }
    </script>
@stop
