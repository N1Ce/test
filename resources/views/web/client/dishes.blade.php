@foreach($dishes as $dish)
    <div style="width: 31%" class="m-1 list-group" id="dishes">
        <div style="height: 150px; background-image: url('{{ url(empty($dish->getImagePath()) ? 'images/2.png' : $dish->getImagePath()) }}'); background-size: cover; background-position: center" class="list-group-item list-group-item-action">
        </div>
        <div class="list-group-item">
            <a href="{{ url('dish'.$dish->getKey()) }}">{{ $dish->getName() }}</a>
            <div class="d-flex justify-content-between">
                <span>{{ $dish->getPrice() }} грн</span>
                <span><a href="{{ url('addBasket/'.$dish->getKey()) }}"><i class="fas fa-shopping-cart" style="font-size:25px"></i></a></span>
            </div>
        </div>
    </div>
@endforeach
