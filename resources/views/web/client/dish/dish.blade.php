@extends('layouts.client.app')

@section('content')
    <div class="row">
        <div class="col-sm-2 m-1">
            @include('layouts.client.includes.menu.left')
        </div>
        <div class="col-sm-9">
            <div class="slick-slider">
                @if($dish->getRelationImages()->isNotEmpty())
                    @foreach($dish->getRelationImages() as $image)
                        <div style="height: 500px; background-image: url('{{ asset($image->getPath()) }}'); background-position: center; background-size: cover"></div>
                    @endforeach
                @else
                    <div style="height: 500px; background-image: url('/images/2.png'); background-position: center; background-size: cover"></div>
                @endif
            </div>
            <div>
                <h1 class="text-center">{{ $dish->getName() }}</h1>
                <span>Ingredients:</span>
                <p></p>
                <span>Description:</span>
                <p>{{ $dish->getDescription() }}</p>
            </div>
            <div>
                <a href="{{ url('addBasket/'.$dish->getID()) }}"><i class="fas fa-shopping-cart"
                                                                    style="font-size:25px"></i></a>
            </div>
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
    <script type="text/javascript">
        $(".slick-slider").slick({
            autoplay: true,
            arrows: false
        });
    </script>
@stop
