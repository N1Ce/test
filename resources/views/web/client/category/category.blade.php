@extends('layouts.client.app')

@section('content')
<div class="row">
    <div class="col-sm-2 m-1">
        @include('layouts.client.includes.menu.left')
    </div>
    <div class="col-sm-9">
        <div class="d-flex flex-wrap justify-content-between" id="dishes">
            @include('web.client.dishes')
        </div>
    </div>
</div>
<div class="ajax-load text-center" style="display:none">
    <p><img src="http://demo.itsolutionstuff.com/plugin/loader.gif">Loading More post</p>
</div>
@stop

@section('scripts')
    <script type="text/javascript">
        var page = 1;
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() >= $(document).height()) {
                page++;
                loadMoreData(page);
            }
        });


        function loadMoreData(page){
            $.ajax(
                {
                    url: '?page=' + page,
                    type: "get",
                    beforeSend: function()
                    {
                        $('.ajax-load').show();
                    }
                })
                .done(function(data)
                {
                    if(data == " "){
                        $('.ajax-load').html("No more records found");
                        return;
                    }
                    $('.ajax-load').hide();
                    $("#dish").append(data);
                })
                .fail(function(jqXHR, ajaxOptions, thrownError)
                {
                    alert('server not responding...');
                });
        }
    </script>
@endSection
