<?php

namespace App\Domain\User\Contracts;

use App\Domain\User\User;

interface UserRepository
{
    /**
     * @param array $arguments
     *
     * @return User
     */
    public function create(array $arguments): User;


    /**
     * @param string $email
     *
     * @return bool
     */
    public function checkEmail(string $email): bool;
}
