<?php

namespace App\Domain\IngredientsItem;

use Illuminate\Database\Eloquent\Model;

class IngredientItem extends Model
{
    public const NAME = 'name';

    protected string $table = 'ingredients_item';

    protected array $fillable = [
        self::NAME
    ];

    public function getName(): string
    {
        return $this->getAttributeValue(self::NAME);
    }
}
