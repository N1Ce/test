<?php

namespace App\Domain\IngredientsItem\Contracts;

use App\Domain\IngredientsItem\IngredientItem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface IngredientsItemRepository
{
    /**
     * @param int $count
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPagination(int $count = 10): LengthAwarePaginator;

    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @param string $name
     *
     * @return IngredientItem
     */
    public function create(string $name): IngredientItem;

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param int    $id
     * @param string $name
     *
     * @return IngredientItem
     */
    public function update(int $id, string $name): IngredientItem;

    /**
     * @param int $id
     *
     * @return IngredientItem
     */
    public function getByID(int $id): IngredientItem;
}
