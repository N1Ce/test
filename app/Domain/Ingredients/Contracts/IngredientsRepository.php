<?php

namespace App\Domain\Ingredients\Contracts;

use App\Domain\Ingredients\Ingredients;

interface IngredientsRepository
{
    public function create(int $dishID, int $ingredientID): Ingredients;
}
