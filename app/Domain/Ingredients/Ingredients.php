<?php

namespace App\Domain\Ingredients;

use App\Domain\Dish\Dish;
use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    public const DISH_ID = 'dish_id';
    public const INGREDIENTS_ITEM_ID = 'ingredients_item_id';

    protected string $table = 'ingredients';

    public $timestamps = false;

    protected array $fillable = [
        self::DISH_ID,
        self::INGREDIENTS_ITEM_ID,
    ];

    public function dish()
    {
        return $this->belongsToMany(Dish::class, 'id');
    }
}
