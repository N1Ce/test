<?php

namespace App\Domain\Images\Contracts;

use Illuminate\Database\Eloquent\Model;

interface ImagesRepository
{
    /**
     * @param string $path
     * @param Model  $model
     *
     * @return Model
     */
    public function associate(string $path, Model $model): Model;

    /**
     * @param int    $id
     * @param string $model
     */
    public function delete(int $id, string $model): void;
}
