<?php

namespace App\Domain\Images;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    public const PATH = 'path';
    public const IMAGEABLE_ID = 'imageable_id';
    public const IMAGEABLE_TYPE = 'imageable_type';

    protected string $table = 'images';

    protected array $fillable = [
        self::PATH,
        self::IMAGEABLE_ID,
        self::IMAGEABLE_TYPE
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function imageable()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->getAttributeValue(self::PATH) ?? '';
    }
}
