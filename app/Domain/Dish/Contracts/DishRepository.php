<?php

namespace App\Domain\Dish\Contracts;

use App\Domain\Dish\Dish;
use App\Domain\Dish\Request\Create;
use App\Infrastructure\Repositories\Exception\RepositoryException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface DishRepository
{
    /**
     * @return LengthAwarePaginator
     */
    public function getDishes(): LengthAwarePaginator;

    /**
     * @param int $id
     *
     * @return LengthAwarePaginator
     */
    public function getDishesByCategoryID(int $id): LengthAwarePaginator;

    /**
     * @param int $id
     *
     * @return Dish|null
     */
    public function getDishByIdAndImages(int $id): ?Dish;

    /**
     * @param int $id
     *
     * @return Dish|null
     */
    public function getDishById(int $id): ?Dish;

    /**
     * @param int $id
     *
     * @return Dish
     */
    public function getDishWithImage(int $id): Dish;

    /**
     * @param array $array
     *
     * @return Collection
     */
    public function getDishesByArray(array $array): Collection;

    /**
     * @param Create $create
     *
     * @return Dish
     */
    public function create(Create $create): Dish;

    /**
     * @param int $id
     *
     * @return Dish
     */
    public function getDishByIdWhitImageAndIngredients(int $id): Dish;

    /**
     * @param int $id
     *
     * @return bool
     * @throws RepositoryException
     */
    public function delete(int $id): bool;
}
