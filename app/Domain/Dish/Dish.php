<?php

namespace App\Domain\Dish;

use App\Domain\Category\Category;
use App\Domain\Images\Images;
use App\Domain\Ingredients\Ingredients;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * @method findOrFail(int $id)
 */
class Dish extends Model
{
    public const ID = 'id';
    public const NAME = 'name';
    public const DESCRIPTION = 'description';
    public const PRICE = 'price';
    public const WEIGHT = 'weight';
    public const CATEGORY_ID = 'category_id';
    public const IMAGE = 'image';
    public const IMAGES = 'images';
    public const CATEGORY = 'category';
    public const INGREDIENTS = 'ingredients';

    protected string $table = 'dishes';

    protected array $fillable = [
        self::CATEGORY_ID,
        self::NAME,
        self::DESCRIPTION,
        self::PRICE,
        self::WEIGHT,
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->morphMany(Images::class, 'imageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Images::class, 'imageable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ingredients()
    {
        return $this->hasMany(Ingredients::class, 'dish_id');
    }

    /**
     * @return Model|\Illuminate\Database\Eloquent\Relations\BelongsTo|object|null
     */
    public function category()
    {
        return $this->belongsTo(Category::class, Dish::CATEGORY_ID);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttributeValue(self::NAME);
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->getAttributeValue(self::PRICE);
    }

    public function getRelationImages(): ?Collection
    {
        return $this->getRelation(self::IMAGES);
    }

    public function getRelationImage(): ?Images
    {
        return $this->getRelation(self::IMAGE);
    }

    public function getRelationCategory()
    {
        return $this->getRelation(self::CATEGORY);
    }

    /**
     * @return string
     */
    public function getImagePath(): string
    {
        if (is_null($this->getRelationImage())) {
            return '';
        }

        return $this->getRelationImage()->getPath();
    }

    public function getID(): int
    {
        return $this->getAttributeValue(self::ID);
    }

    public function getDescription(): string
    {
        return $this->getAttributeValue(self::DESCRIPTION);
    }

    public function getCategoryId(): int
    {
        return $this->getAttributeValue(self::CATEGORY_ID);
    }

    public function getCategoryName(): string
    {
        return $this->getRelationCategory()->getName();
    }

    public function getWeight(): int
    {
        return $this->getAttributeValue(self::WEIGHT);
    }
}
