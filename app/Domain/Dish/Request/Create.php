<?php

namespace App\Domain\Dish\Request;

use App\Domain\Dish\Dish;

class Create
{
    private const NAME = 'name';
    private const CATEGORY = 'category';
    private const PRICE = 'price';
    private const WEIGHT = 'weight';
    private const DESCRIPTION = 'description';

    /** @var string $name */
    private string $name;

    /** @var int $categoryID */
    private int $categoryID;

    /** @var float $price */
    private float $price;

    /** @var int $weight */
    private int $weight;

    /** @var string $description */
    private string $description;

    /**
     * Request constructor.
     *
     * @param array $array
     */
    public function __construct(array $array)
    {
        $this->name = data_get($array, self::NAME);
        $this->categoryID = (int)data_get($array, self::CATEGORY);
        $this->price = (float)data_get($array, self::PRICE);
        $this->weight = (int)data_get($array, self::WEIGHT);
        $this->description = data_get($array, self::DESCRIPTION);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCategoryID(): int
    {
        return $this->categoryID;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    public function toArrayByDish(): array
    {
        return [
            Dish::NAME => $this->getName(),
            Dish::PRICE => $this->getPrice(),
            Dish::CATEGORY_ID => $this->getCategoryID(),
            Dish::WEIGHT => $this->getWeight(),
            Dish::DESCRIPTION => $this->getDescription()
        ];
    }
}
