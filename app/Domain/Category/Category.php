<?php

namespace App\Domain\Category;

use Illuminate\Database\Eloquent\Model;

final class Category extends Model
{
    public const NAME = 'name';
    public const ID = 'id';

    protected string $table = 'categories';

    protected array $fillable = [
        self::NAME
    ];

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->getAttributeValue(self::ID);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getAttributeValue(self::NAME);
    }

    /**
     * @param string $name
     *
     * @return Category
     */
    public function setName(string $name): self
    {
        return $this->setAttribute(self::NAME, $name);
    }
}
