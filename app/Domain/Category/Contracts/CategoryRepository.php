<?php

namespace App\Domain\Category\Contracts;

use App\Domain\Category\Category;
use Illuminate\Database\Eloquent\Collection;

interface CategoryRepository
{
    /**
     * @return Collection
     */
    public function getAll(): Collection;

    /**
     * @param string $name
     *
     * @return Category
     */
    public function create(string $name): Category;

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool;

    /**
     * @param int $id
     *
     * @return Category
     */
    public function getById(int $id): Category;

    /**
     * @param int    $id
     * @param string $name
     *
     * @return Category
     */
    public function update(int $id, string $name): Category;
}
