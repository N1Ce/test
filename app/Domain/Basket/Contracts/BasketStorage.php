<?php

namespace App\Domain\Services\Basket\Contracts;

use App\Domain\Basket\Basket;

interface BasketStorage
{
    /**
     * @return Basket
     */
    public function getBasket(): Basket;

    /**
     * @param Basket $basket
     */
    public function saveBasket(Basket $basket): void;
}
