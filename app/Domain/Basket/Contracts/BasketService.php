<?php

namespace App\Domain\Basket\Contracts;

use App\Domain\Basket\Basket;
use App\Domain\Dish\Dish;

interface BasketService
{
    public function addBasket(Dish $dish): void;

    /**
     * @return Basket
     */
    public function getBasket(): Basket;

    public function removeItem(Dish $dish): void;

    public function saveBasket(Basket $basket): void;
}
