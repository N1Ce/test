<?php

namespace App\Domain\Services\Basket;

use App\Domain\Basket\Basket;
use App\Domain\Basket\Item;
use App\Domain\Basket\Contracts\BasketService as ContractBasketService;
use App\Domain\Dish\Dish;
use App\Domain\Services\Basket\Contracts\BasketStorage;

final class BasketService implements ContractBasketService
{
    private BasketStorage $basketStorage;

    public function __construct(BasketStorage $basketStorage)
    {
        $this->basketStorage = $basketStorage;
    }

    /**
     * @param Dish $dish
     */
    public function addBasket(Dish $dish): void
    {
        $basket = $this->getBasket();
        $basket->setItem(new Item($dish));
        $this->basketStorage->saveBasket($basket);
    }

    /**
     * @return Basket
     */
    public function getBasket(): Basket
    {
        $basket = $this->basketStorage->getBasket();

        return $basket;
    }

    public function removeItem(Dish $dish): void
    {
        $basket = $this->getBasket();
        $basket->removeItem($dish->getkey());
        $this->basketStorage->saveBasket($basket);
    }

    public function saveBasket(Basket $basket): void
    {
        $this->basketStorage->saveBasket($basket);
    }
}
