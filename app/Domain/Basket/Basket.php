<?php

namespace App\Domain\Basket;

final class Basket
{
    /** @var Item[] $items */
    private array $items = [];

    public function __construct(array $items = [])
    {
        $this->items = $items;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item $item
     *
     * @return Basket
     */
    public function setItem(Item $item): self
    {
        $this->items[] = $item;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotalQty(): int
    {
        return count($this->items);
    }

    /**
     * @return float|int
     */
    public function calculateSum()
    {
        return array_sum(
            array_map(
                function (Item $item) {
                    return $item->getPrice();
                },
                $this->items
            )
        );
    }

    /**
     * @param int $key
     *
     * @return Basket
     */
    public function removeItem(int $key): self
    {
        $key = array_search(
            $key,
            array_map(
                function (Item $item) {
                    return $item->getProductID();
                },
                $this->getItems()
            )
        );

        unset($this->items[$key]);

        return $this;
    }

    /**
     * @param int $id
     *
     * @return Basket
     */
    public function removeItems(int $id): self
    {
        foreach ($this->items as $key => $item) {
            if ($item->getProductID() == $id) {
                unset($this->items[$key]);
            }
        }

        return $this;
    }
}
