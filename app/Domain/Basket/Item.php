<?php

namespace App\Domain\Basket;

use App\Domain\Dish\Dish;

final class Item
{
    private int $productID;

    private float $price = 0;

    private string $productName;

    public function __construct(Dish $dish)
    {
        $this->productID = $dish->getKey();
        $this->price = (float) $dish->getPrice();
        $this->productName = $dish->getName();
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName ?? '';
    }

    /**
     * @return int|null
     */
    public function getProductID(): ?int
    {
        return $this->productID ?? null;
    }
}
