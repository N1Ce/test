<?php

namespace App\Presentation\Controllers\Admin\Dish\Create;

use App\Application\UseCases\Admins\Dish\Create\Contracts\CreateDishCase;
use App\Application\UseCases\Admins\Dish\Create\Request\Request;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Dish\Contracts\CreateDishPresenter;
use App\Presentation\Requests\Dish\CreateDishRequest;

class CreateDishController extends Controller
{
    private CreateDishCase $case;

    private CreateDishPresenter $presenter;

    public function __construct(CreateDishCase $case, CreateDishPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(CreateDishRequest $request)
    {
        $request = new Request($request->validated(), $request->file('file'));
        $this->case->execute($request);

        return $this->presenter->present();
    }
}
