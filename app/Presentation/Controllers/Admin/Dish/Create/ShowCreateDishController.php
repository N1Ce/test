<?php

namespace App\Presentation\Controllers\Admin\Dish\Create;

use App\Application\UseCases\Admins\Dish\Create\Contracts\ShowCreateDishCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Dish\Contracts\ShowCreateDishPresenter;

class ShowCreateDishController extends Controller
{
    private ShowCreateDishCase $case;

    private ShowCreateDishPresenter $presenter;

    public function __construct(ShowCreateDishCase $case, ShowCreateDishPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $response = $this->case->handler();

        return $this->presenter->present($response);
    }
}
