<?php

namespace App\Presentation\Controllers\Admin\Dish\Show;

use App\Application\UseCases\Admins\Dish\Show\Contracts\ShowDishCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Dish\Contracts\ShowDishPresenter;

class ShowDishController extends Controller
{
    private ShowDishCase $case;

    private ShowDishPresenter $presenter;

    public function __construct(ShowDishCase $case, ShowDishPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $dishes = $this->case->getDishes();

        return $this->presenter->present($dishes);
    }
}
