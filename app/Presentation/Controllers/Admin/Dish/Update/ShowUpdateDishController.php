<?php

namespace App\Presentation\Controllers\Admin\Dish\Update;

use App\Application\UseCases\Admins\Dish\Update\Contracts\ShowUpdateDishCase;
use App\Presentation\Controllers\Controller;

class ShowUpdateDishController extends Controller
{
    private ShowUpdateDishCase $case;

    public function __construct(ShowUpdateDishCase $case)
    {
        $this->case = $case;
    }

    public function __invoke(int $id)
    {
        $dish = $this->case->execute($id);
        dd($dish);
    }
}
