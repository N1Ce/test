<?php

namespace App\Presentation\Controllers\Admin\Dish\Delete;

use App\Application\UseCases\Admins\Dish\Delete\Contracts\DeleteDishCase;
use App\Application\UseCases\Admins\Dish\Exception\DishException;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Dish\Contracts\DeleteDishPresenter;
use App\Presentation\Requests\Dish\DeleteDishRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DeleteDishController extends Controller
{
    private DeleteDishCase $case;

    private DeleteDishPresenter $presenter;

    public function __construct(DeleteDishCase $case, DeleteDishPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(DeleteDishRequest $request)
    {
        $id = data_get($request->validated(), DeleteDishRequest::ID);
        try {
            $this->case->execute($id);
        } catch (DishException $exception) {
            throw new ModelNotFoundException($exception->getMessage());
        }


        return $this->presenter->present();
    }
}
