<?php

namespace App\Presentation\Controllers\Admin\Category\Update;

use App\Application\UseCases\Admins\Category\Update\Contracts\ShowUpdateCategoryCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Category\Contracts\ShowUpdateCategoryPresenter;

class ShowUpdateCategoryController extends Controller
{
    private ShowUpdateCategoryCase $case;

    private ShowUpdateCategoryPresenter $presenter;

    public function __construct(ShowUpdateCategoryCase $case, ShowUpdateCategoryPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $id)
    {
        $category = $this->case->getCategory($id);

        return $this->presenter->present($category);
    }
}
