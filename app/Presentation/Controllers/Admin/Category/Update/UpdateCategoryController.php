<?php

namespace App\Presentation\Controllers\Admin\Category\Update;

use App\Application\UseCases\Admins\Category\Update\Contracts\UpdateCategoryCase;
use App\Application\UseCases\Admins\Category\Update\Request\UpdateCategory;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Category\Contracts\UpdateCategoryPresenter;
use App\Presentation\Requests\Category\UpdateCategoryRequest;

class UpdateCategoryController extends Controller
{
    private UpdateCategoryCase $case;

    private UpdateCategoryPresenter $presenter;

    public function __construct(UpdateCategoryCase $case, UpdateCategoryPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(UpdateCategoryRequest $request)
    {
        $request = new UpdateCategory($request->validated());
        $this->case->updateCategory($request);

        return $this->presenter->present();
    }
}
