<?php

namespace App\Presentation\Controllers\Admin\Category\Delete;

use App\Application\UseCases\Admins\Category\Delete\Contracts\DeleteCategoryCase;
use App\Application\UseCases\Admins\Category\Delete\Request\DeleteCategory;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Category\Contracts\DeleteCategoryPresenter;
use App\Presentation\Requests\Category\DeleteCategoryRequest;

class DeleteCategoryController extends Controller
{
    private DeleteCategoryCase $case;

    private DeleteCategoryPresenter $presenter;

    public function __construct(DeleteCategoryCase $case, DeleteCategoryPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(DeleteCategoryRequest $request)
    {
        $request = new DeleteCategory($request->validated());
        $this->case->deleteCategory($request);

        return $this->presenter->present();
    }
}
