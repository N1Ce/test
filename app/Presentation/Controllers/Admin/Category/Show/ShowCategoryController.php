<?php

namespace App\Presentation\Controllers\Admin\Category\Show;

use App\Application\UseCases\Admins\Category\Show\Contracts\ShowCategoryCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Category\Contracts\ShowCategoryPresenter;

class ShowCategoryController extends Controller
{
    private ShowCategoryCase $case;

    private ShowCategoryPresenter $presenter;

    public function __construct(ShowCategoryCase $case, ShowCategoryPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $categories = $this->case->getCategories();

        return $this->presenter->present($categories);
    }
}
