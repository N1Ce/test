<?php

namespace App\Presentation\Controllers\Admin\Category\Create;

use App\Application\UseCases\Admins\Category\Create\Contracts\CreateCategoryCase;
use App\Application\UseCases\Admins\Category\Create\Request\CreateCategory;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Category\Contracts\CreateCategoryPresenter;
use App\Presentation\Requests\Category\CreateCategoryRequest;

class CreateCategoryController extends Controller
{
    private CreateCategoryCase $case;

    private CreateCategoryPresenter $presenter;

    public function __construct(CreateCategoryCase $case, CreateCategoryPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(CreateCategoryRequest $request)
    {
        $request = new CreateCategory($request->validated());
        $this->case->createCategory($request);

        return $this->presenter->present();
    }
}
