<?php

namespace App\Presentation\Controllers\Admin\Category\Create;

use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Category\Contracts\ShowCreateCategoryPresenter;

class ShowCreateCategoryController extends Controller
{
    private ShowCreateCategoryPresenter $presenter;

    public function __construct(ShowCreateCategoryPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        return $this->presenter->present();
    }
}
