<?php

namespace App\Presentation\Controllers\Admin\Ingredients\Update;

use App\Application\UseCases\Admins\Ingredients\Update\Contracts\UpdateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Update\Request\Update;
use App\Presentation\Representation\Admin\Ingredients\Contracts\UpdateIngredientPresenter;
use App\Presentation\Requests\Ingredients\UpdateIngredientRequest;

final class UpdateIngredientController
{
    private UpdateIngredientCase $case;

    private UpdateIngredientPresenter $presenter;

    public function __construct(UpdateIngredientCase $case, UpdateIngredientPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(UpdateIngredientRequest $request)
    {
        $update = new Update($request->validated());
        $this->case->updateIngredients($update);

        return $this->presenter->present();
    }
}
