<?php

namespace App\Presentation\Controllers\Admin\Ingredients\Update;

use App\Application\UseCases\Admins\Ingredients\Update\Contracts\ShowUpdateIngredientCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowUpdateIngredientPresenter;

final class ShowUpdateIngredientController extends Controller
{
    private ShowUpdateIngredientCase $case;

    private ShowUpdateIngredientPresenter $presenter;

    public function __construct(ShowUpdateIngredientCase $case, ShowUpdateIngredientPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $id)
    {
        $ingredient = $this->case->getIngredient($id);

        return $this->presenter->present($ingredient);
    }
}
