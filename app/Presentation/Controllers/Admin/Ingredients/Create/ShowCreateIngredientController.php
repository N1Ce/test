<?php

namespace App\Presentation\Controllers\Admin\Ingredients\Create;

use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowCreateIngredientPresenter;

class ShowCreateIngredientController
{
    private ShowCreateIngredientPresenter $presenter;

    public function __construct(ShowCreateIngredientPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        return $this->presenter->present();
    }
}
