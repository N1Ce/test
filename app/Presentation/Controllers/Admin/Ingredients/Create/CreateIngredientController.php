<?php

namespace App\Presentation\Controllers\Admin\Ingredients\Create;

use App\Application\UseCases\Admins\Ingredients\Create\Contracts\CreateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Create\Request\Ingredient;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Ingredients\Contracts\CreateIngredientPresenter;
use App\Presentation\Requests\Ingredients\CreateIngredientRequest;

class CreateIngredientController extends Controller
{
    private CreateIngredientCase $case;

    private CreateIngredientPresenter $presenter;

    public function __construct(CreateIngredientCase $case, CreateIngredientPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(CreateIngredientRequest $request)
    {
        $ingredient = new Ingredient($request->validated());
        $this->case->createIngredient($ingredient);

        return $this->presenter->present();
    }
}
