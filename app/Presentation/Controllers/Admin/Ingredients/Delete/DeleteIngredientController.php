<?php

namespace App\Presentation\Controllers\Admin\Ingredients\Delete;

use App\Application\UseCases\Admins\Ingredients\Delete\Contracts\DeleteIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Delete\Request\Delete;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Ingredients\Contracts\DeleteIngredientPresenter;
use App\Presentation\Requests\Ingredients\DeleteIngredientRequest;

final class DeleteIngredientController extends Controller
{
    private DeleteIngredientCase $case;

    private DeleteIngredientPresenter $presenter;

    public function __construct(DeleteIngredientCase $case, DeleteIngredientPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(DeleteIngredientRequest $request)
    {
        $ingredient = new Delete($request->validated());
        $this->case->deleteIngredient($ingredient);

        return $this->presenter->present();
    }
}
