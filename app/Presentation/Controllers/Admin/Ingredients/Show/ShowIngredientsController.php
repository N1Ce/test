<?php

namespace App\Presentation\Controllers\Admin\Ingredients\Show;

use App\Application\UseCases\Admins\Ingredients\Show\Contracts\ShowIngredientsCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Ingredients\ShowIngredientsPresenter;

class ShowIngredientsController extends Controller
{
    private ShowIngredientsCase $case;

    private ShowIngredientsPresenter $presenter;

    public function __construct(ShowIngredientsCase $case, ShowIngredientsPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $ingredients = $this->case->getIngredients();

        return $this->presenter->present($ingredients);
    }
}
