<?php

namespace App\Presentation\Controllers\Admin\Dashboard;

use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Admin\Dashboard\Contracts\DashboardPresenter;

final class DashboardController extends Controller
{
    private DashboardPresenter $presenter;

    public function __construct(DashboardPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        return $this->presenter->present();
    }
}
