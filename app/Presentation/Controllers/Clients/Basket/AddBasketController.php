<?php

namespace App\Presentation\Controllers\Clients\Basket;

use App\Presentation\Controllers\Controller;
use App\Application\UseCases\Clients\Basket\Contracts\AddBasketCase;
use App\Presentation\Representation\Clients\Basket\Contracts\AddBasketPresenter;

final class AddBasketController extends Controller
{
    private AddBasketCase $case;

    private AddBasketPresenter $presenter;

    public function __construct(AddBasketCase $case, AddBasketPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $id)
    {
        $this->case->execute($id);

        return $this->presenter->present();
    }
}
