<?php

namespace App\Presentation\Controllers\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\RemoveBasketItemCase;
use App\Presentation\Representation\Clients\Basket\Contracts\RemoveBasketPresenter;

final class RemoveBasketController
{
    private RemoveBasketItemCase $case;

    private RemoveBasketPresenter $presenter;

    public function __construct(RemoveBasketItemCase $case, RemoveBasketPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $id)
    {
        $this->case->execute($id);

        return $this->presenter->present();
    }
}
