<?php

namespace App\Presentation\Controllers\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\RemoveItemsByIdCase;
use App\Presentation\Controllers\Controller;

final class RemoveBasketItemsController extends Controller
{
    private RemoveItemsByIdCase $case;

    public function __construct(RemoveItemsByIdCase $case)
    {
        $this->case = $case;
    }

    public function __invoke(int $id)
    {
        $this->case->execute($id);
    }
}
