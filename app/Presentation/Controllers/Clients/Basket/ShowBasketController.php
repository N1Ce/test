<?php

namespace App\Presentation\Controllers\Clients\Basket;

use App\Presentation\Controllers\Controller;
use App\Application\UseCases\Clients\Basket\Contracts\ShowBasketCase;
use App\Presentation\Representation\Clients\Basket\Contracts\ShowBasketPresenter;

final class ShowBasketController extends Controller
{
    private ShowBasketCase $case;

    private ShowBasketPresenter $presenter;

    public function __construct(ShowBasketCase $case, ShowBasketPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke()
    {
        $basket = $this->case->execute();

        return $this->presenter->present($basket);
    }
}
