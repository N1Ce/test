<?php

namespace App\Presentation\Controllers\Clients;

use App\Application\UseCases\Clients\Home\Contracts\HomeCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Clients\Home\Contracts\HomePresenter;
use Illuminate\Http\Request;

final class HomeController extends Controller
{
    private HomeCase $case;

    private HomePresenter $presenter;

    /**
     * HomeController constructor.
     *
     * @param HomeCase      $case
     * @param HomePresenter $presenter
     */
    public function __construct(HomeCase $case, HomePresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(Request $request)
    {
        $response = $this->case->execute();

        return $this->presenter->present($response, $request);
    }
}
