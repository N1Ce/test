<?php

namespace App\Presentation\Controllers\Clients\Category;

use App\Application\UseCases\Clients\Category\Contracts\ShowCategoryCase;
use App\Presentation\Representation\Clients\Category\Contracts\ShowCategoryPresenter;
use App\Presentation\Controllers\Controller;
use Illuminate\Http\Request;

final class CategoryController extends Controller
{
    private ShowCategoryCase $case;

    private ShowCategoryPresenter $presenter;

    public function __construct(ShowCategoryCase $case, ShowCategoryPresenter $presenter)
    {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $id, Request $request)
    {
        $dishes = $this->case->execute($id);

        return $this->presenter->present($dishes, $request);
    }
}
