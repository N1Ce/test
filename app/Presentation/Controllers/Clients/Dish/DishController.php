<?php

namespace App\Presentation\Controllers\Clients\Dish;

use App\Application\UseCases\Clients\Dish\Contracts\ShowDishCase;
use App\Presentation\Controllers\Controller;
use App\Presentation\Representation\Clients\Dish\Contracts\ShowDishPresenter;

final class DishController extends Controller
{
    private ShowDishCase $case;

    private ShowDishPresenter $presenter;

    public function __construct(
        ShowDishCase $case,
        ShowDishPresenter $presenter
    ) {
        $this->case = $case;
        $this->presenter = $presenter;
    }

    public function __invoke(int $id)
    {
        $dish = $this->case->execute($id);

        return $this->presenter->present($dish);
    }
}
