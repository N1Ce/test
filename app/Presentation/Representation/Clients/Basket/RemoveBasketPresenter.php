<?php

namespace App\Presentation\Representation\Clients\Basket;

use App\Presentation\Representation\Clients\Basket\Contracts\RemoveBasketPresenter as ContractRemoveBasketPresenter;

final class RemoveBasketPresenter implements ContractRemoveBasketPresenter
{
    public function present()
    {
        return back();
    }
}
