<?php

namespace App\Presentation\Representation\Clients\Basket;

use App\Presentation\Representation\Clients\Basket\Contracts\AddBasketPresenter as ContractAddBasketPresenter;

final class AddBasketPresenter implements ContractAddBasketPresenter
{
    public function present()
    {
        return back();
    }
}
