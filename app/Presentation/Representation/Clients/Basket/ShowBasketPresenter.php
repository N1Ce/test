<?php

namespace App\Presentation\Representation\Clients\Basket;

use App\Presentation\Representation\Clients\Basket\Contracts\ShowBasketPresenter as ContractShowBasketPresenter;
use Illuminate\Support\Collection;

final class ShowBasketPresenter implements ContractShowBasketPresenter
{
    public function present(Collection $basket)
    {
        return view(
            'web.client.basket.show',
            [
                'basket' => $basket,
            ]
        );
    }
}
