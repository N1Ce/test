<?php

namespace App\Presentation\Representation\Clients\Basket\Contracts;

interface RemoveBasketPresenter
{
    public function present();
}
