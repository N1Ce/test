<?php

namespace App\Presentation\Representation\Clients\Basket\Contracts;

interface AddBasketPresenter
{
    public function present();
}
