<?php

namespace App\Presentation\Representation\Clients\Basket\Contracts;

use Illuminate\Support\Collection;

interface ShowBasketPresenter
{
    public function present(Collection $basket);
}
