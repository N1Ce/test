<?php

namespace App\Presentation\Representation\Clients\Category\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface ShowCategoryPresenter
{
    public function present(LengthAwarePaginator $response, Request $request);
}
