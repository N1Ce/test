<?php

namespace App\Presentation\Clients\Category;

use App\Presentation\Representation\Clients\Category\Contracts\ShowCategoryPresenter as ContractShowCategoryPresenter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class ShowCategoryPresenter implements ContractShowCategoryPresenter
{
    public function present(LengthAwarePaginator $response, Request $request)
    {
        if ($request->ajax()) {
            return view(
                'web.client.dish',
                [
                    'dish' => $response,
                ]
            )->render();
        }

        return view(
            'web.client.category.category',
            [
                'dish' => $response,
            ]
        );
    }
}
