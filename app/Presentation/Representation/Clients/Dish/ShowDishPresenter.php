<?php

namespace App\Presentation\Representation\Clients\Dish;

use App\Domain\Dish\Dish;
use App\Presentation\Representation\Clients\Dish\Contracts\ShowDishPresenter as ContractShowDishPresenter;

class ShowDishPresenter implements ContractShowDishPresenter
{
    public function present(?Dish $dish)
    {
        if (is_null($dish)) {
            return abort(404);
        }

        return view(
            'web.client.dishes.dishes',
            [
                'dishes' => $dish
            ]
        );
    }
}
