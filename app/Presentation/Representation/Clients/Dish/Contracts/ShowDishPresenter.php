<?php

namespace App\Presentation\Representation\Clients\Dish\Contracts;

use App\Domain\Dish\Dish;

interface ShowDishPresenter
{
    public function present(?Dish $dish);
}
