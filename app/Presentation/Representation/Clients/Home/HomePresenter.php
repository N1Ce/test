<?php

namespace App\Presentation\Clients\Home;

use App\Presentation\Representation\Clients\Home\Contracts\HomePresenter as ContractHomePresenter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class HomePresenter implements ContractHomePresenter
{
    public function present(LengthAwarePaginator $response, Request $request)
    {
        if ($request->ajax()) {
            return view(
                'web.client.dishes',
                [
                    'dishes' => $response,
                ]
            )->render();
        }

        return view(
            'web.client.home',
            [
                'dishes' => $response,
            ]
        );
    }
}