<?php

namespace App\Presentation\Representation\Clients\Home\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

interface HomePresenter
{
    public function present(LengthAwarePaginator $response, Request $request);
}
