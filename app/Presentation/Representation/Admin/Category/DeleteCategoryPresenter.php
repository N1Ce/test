<?php

namespace App\Presentation\Representation\Admin\Category;

use App\Presentation\Representation\Admin\Category\Contracts\DeleteCategoryPresenter as ContractDeleteCategoryPresenter;
use Illuminate\Http\RedirectResponse;

class DeleteCategoryPresenter implements ContractDeleteCategoryPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/category');
    }
}
