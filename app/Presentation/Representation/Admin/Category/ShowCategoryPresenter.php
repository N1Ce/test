<?php

namespace App\Presentation\Representation\Admin\Category;

use App\Presentation\Representation\Admin\Category\Contracts\ShowCategoryPresenter
    as ContractShowCategoryPresenter;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;

class ShowCategoryPresenter implements ContractShowCategoryPresenter
{
    public function present(Collection $categories): View
    {
        return view(
            'web.admin.dashboard.category.show_category',
            [
                'categories' => $categories,
            ]
        );
    }
}
