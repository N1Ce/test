<?php

namespace App\Presentation\Representation\Admin\Category\Contracts;

use Illuminate\Http\RedirectResponse;

interface UpdateCategoryPresenter
{
    public function present(): RedirectResponse;
}
