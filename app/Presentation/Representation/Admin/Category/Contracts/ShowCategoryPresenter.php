<?php

namespace App\Presentation\Representation\Admin\Category\Contracts;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;

interface ShowCategoryPresenter
{
    public function present(Collection $categories): View;
}
