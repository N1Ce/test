<?php

namespace App\Presentation\Representation\Admin\Category\Contracts;

use App\Domain\Category\Category;
use Illuminate\Contracts\View\View;

interface ShowUpdateCategoryPresenter
{
    public function present(Category $category): View;
}
