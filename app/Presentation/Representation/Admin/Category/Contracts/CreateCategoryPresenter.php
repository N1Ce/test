<?php

namespace App\Presentation\Representation\Admin\Category\Contracts;

use Illuminate\Http\RedirectResponse;

interface CreateCategoryPresenter
{
    public function present(): RedirectResponse;
}
