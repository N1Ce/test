<?php

namespace App\Presentation\Representation\Admin\Category\Contracts;

use Illuminate\Contracts\View\View;

interface ShowCreateCategoryPresenter
{
    public function present(): View;
}
