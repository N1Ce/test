<?php

namespace App\Presentation\Representation\Admin\Category\Contracts;

use Illuminate\Http\RedirectResponse;

interface DeleteCategoryPresenter
{
    public function present(): RedirectResponse;
}
