<?php

namespace App\Presentation\Representation\Admin\Category;

use App\Presentation\Representation\Admin\Category\Contracts\ShowCreateCategoryPresenter
    as ContractShowCreateCategoryPresenter;
use Illuminate\Contracts\View\View;

class ShowCreateCategoryPresenter implements ContractShowCreateCategoryPresenter
{
    public function present(): View
    {
        return view('web.admin.dashboard.category.form_create_category');
    }
}
