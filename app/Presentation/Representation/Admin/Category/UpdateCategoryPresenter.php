<?php

namespace App\Presentation\Representation\Admin\Category;

use App\Presentation\Representation\Admin\Category\Contracts\UpdateCategoryPresenter as ContractUpdateCategoryPresenter;
use Illuminate\Http\RedirectResponse;

class UpdateCategoryPresenter implements ContractUpdateCategoryPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/category');
    }
}
