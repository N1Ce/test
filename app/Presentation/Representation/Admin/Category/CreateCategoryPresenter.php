<?php

namespace App\Presentation\Representation\Admin\Category;

use App\Presentation\Representation\Admin\Category\Contracts\CreateCategoryPresenter
    as ContractCreateCategoryPresenter;
use Illuminate\Http\RedirectResponse;

class CreateCategoryPresenter implements ContractCreateCategoryPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/category');
    }
}
