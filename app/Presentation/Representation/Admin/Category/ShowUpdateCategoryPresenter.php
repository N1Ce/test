<?php

namespace App\Presentation\Representation\Admin\Category;

use App\Domain\Category\Category;
use App\Presentation\Representation\Admin\Category\Contracts\ShowUpdateCategoryPresenter
    as ContractShowUpdateCategoryPresenter;
use Illuminate\Contracts\View\View;

class ShowUpdateCategoryPresenter implements ContractShowUpdateCategoryPresenter
{
    public function present(Category $category): View
    {
        return view('web.admin.dashboard.category.form_update_category', compact('category'));
    }
}
