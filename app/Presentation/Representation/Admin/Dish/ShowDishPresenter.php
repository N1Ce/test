<?php

namespace App\Presentation\Representation\Admin\Dish;

use App\Presentation\Representation\Admin\Dish\Contracts\ShowDishPresenter as ContractShowDishPresenter;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\View;

class ShowDishPresenter implements ContractShowDishPresenter
{
    public function present(LengthAwarePaginator $dishes): View
    {
        return view('web.admin.dashboard.dish.show_dish', ['dishes' => $dishes]);
    }
}
