<?php

namespace App\Presentation\Representation\Admin\Dish;

use App\Presentation\Representation\Admin\Dish\Contracts\DeleteDishPresenter as ContractDeleteDishPresenter;
use Illuminate\Http\RedirectResponse;

class DeleteDishPresenter implements ContractDeleteDishPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/dishes');
    }
}
