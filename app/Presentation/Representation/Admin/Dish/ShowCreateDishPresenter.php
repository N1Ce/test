<?php

namespace App\Presentation\Representation\Admin\Dish;

use App\Application\UseCases\Admins\Dish\Create\Response\Response;
use App\Presentation\Representation\Admin\Dish\Contracts\ShowCreateDishPresenter as ContractShowCreateDishPresenter;
use Illuminate\Contracts\View\View;

class ShowCreateDishPresenter implements ContractShowCreateDishPresenter
{
    public function present(Response $response): View
    {
        return view('web.admin.dashboard.dish.form_create_dish', compact('response'));
    }
}
