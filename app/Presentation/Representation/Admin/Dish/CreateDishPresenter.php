<?php

namespace App\Presentation\Representation\Admin\Dish;

use App\Presentation\Representation\Admin\Dish\Contracts\CreateDishPresenter as ContractCreateDishPresenter;
use Illuminate\Http\RedirectResponse;

class CreateDishPresenter implements ContractCreateDishPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/dishes');
    }
}
