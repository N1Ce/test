<?php

namespace App\Presentation\Representation\Admin\Dish\Contracts;

use Illuminate\Http\RedirectResponse;

interface CreateDishPresenter
{
    public function present(): RedirectResponse;
}
