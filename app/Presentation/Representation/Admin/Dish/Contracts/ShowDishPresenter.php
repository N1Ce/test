<?php

namespace App\Presentation\Representation\Admin\Dish\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\View\View;

interface ShowDishPresenter
{
    public function present(LengthAwarePaginator $dishes): View;
}
