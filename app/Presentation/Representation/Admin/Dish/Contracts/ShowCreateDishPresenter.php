<?php

namespace App\Presentation\Representation\Admin\Dish\Contracts;

use App\Application\UseCases\Admins\Dish\Create\Response\Response;
use Illuminate\Contracts\View\View;

interface ShowCreateDishPresenter
{
    public function present(Response $response): View;
}
