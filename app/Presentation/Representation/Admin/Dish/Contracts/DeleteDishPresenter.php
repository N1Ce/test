<?php

namespace App\Presentation\Representation\Admin\Dish\Contracts;

use Illuminate\Http\RedirectResponse;

interface DeleteDishPresenter
{
    public function present(): RedirectResponse;
}
