<?php

namespace App\Presentation\Representation\Admin\Ingredients;

use App\Presentation\Representation\Admin\Ingredients\Contracts\UpdateIngredientPresenter as ContractUpdateIngredientPresenter;
use Illuminate\Http\RedirectResponse;

class UpdateIngredientPresenter implements ContractUpdateIngredientPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/ingredients');
    }
}
