<?php

namespace App\Presentation\Representation\Admin\Ingredients;

use App\Presentation\Representation\Admin\Ingredients\Contracts\DeleteIngredientPresenter as ContractDeleteIngredientPresenter;
use Illuminate\Http\RedirectResponse;

class DeleteIngredientPresenter implements ContractDeleteIngredientPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/ingredients');
    }
}
