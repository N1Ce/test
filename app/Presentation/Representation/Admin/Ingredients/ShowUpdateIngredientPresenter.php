<?php

namespace App\Presentation\Representation\Admin\Ingredients;

use App\Domain\IngredientsItem\IngredientItem;
use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowUpdateIngredientPresenter as ContractShowUpdateIngredientPresenter;
use Illuminate\Contracts\View\View;

class ShowUpdateIngredientPresenter implements ContractShowUpdateIngredientPresenter
{
    public function present(IngredientItem $ingredient): View
    {
        return view('web.admin.dashboard.ingredients.form_update_ingredient', compact('ingredient'));
    }
}
