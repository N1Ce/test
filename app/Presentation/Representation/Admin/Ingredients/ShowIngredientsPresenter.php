<?php

namespace App\Presentation\Representation\Admin\Ingredients;

use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowIngredientsPresenter as ContractsShowIngredientsPresenter;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;

class ShowIngredientsPresenter implements ContractsShowIngredientsPresenter
{
    public function present(LengthAwarePaginator $ingredients): View
    {
        return view('web.admin.dashboard.ingredients.index', compact('ingredients'));
    }
}
