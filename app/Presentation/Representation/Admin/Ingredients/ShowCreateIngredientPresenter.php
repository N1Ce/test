<?php

namespace App\Presentation\Representation\Admin\Ingredients;

use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowCreateIngredientPresenter as ContractShowCreateIngredientPresenter;
use Illuminate\Contracts\View\View;

class ShowCreateIngredientPresenter implements ContractShowCreateIngredientPresenter
{
    public function present(): View
    {
        return view('web.admin.dashboard.ingredients.form_create_ingredient');
    }
}
