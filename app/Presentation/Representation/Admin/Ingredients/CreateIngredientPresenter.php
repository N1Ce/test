<?php

namespace App\Presentation\Representation\Admin\Ingredients;

use App\Presentation\Representation\Admin\Ingredients\Contracts\CreateIngredientPresenter as ContractCreateIngredientPresenter;
use Illuminate\Http\RedirectResponse;

class CreateIngredientPresenter implements ContractCreateIngredientPresenter
{
    public function present(): RedirectResponse
    {
        return redirect('admin/ingredients');
    }
}
