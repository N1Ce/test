<?php

namespace App\Presentation\Representation\Admin\Ingredients\Contracts;

use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;

interface ShowIngredientsPresenter
{
    public function present(LengthAwarePaginator $ingredients): View;
}
