<?php

namespace App\Presentation\Representation\Admin\Ingredients\Contracts;

use Illuminate\Http\RedirectResponse;

interface UpdateIngredientPresenter
{
    public function present(): RedirectResponse;
}
