<?php

namespace App\Presentation\Representation\Admin\Ingredients\Contracts;

use Illuminate\Http\RedirectResponse;

interface DeleteIngredientPresenter
{
    public function present(): RedirectResponse;
}
