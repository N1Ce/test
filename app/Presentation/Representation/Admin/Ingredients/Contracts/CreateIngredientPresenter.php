<?php

namespace App\Presentation\Representation\Admin\Ingredients\Contracts;

use Illuminate\Http\RedirectResponse;

interface CreateIngredientPresenter
{
    public function present(): RedirectResponse;
}
