<?php

namespace App\Presentation\Representation\Admin\Ingredients\Contracts;

use Illuminate\Contracts\View\View;

interface ShowCreateIngredientPresenter
{
    public function present(): View;
}
