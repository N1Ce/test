<?php

namespace App\Presentation\Representation\Admin\Ingredients\Contracts;

use App\Domain\IngredientsItem\IngredientItem;
use Illuminate\Contracts\View\View;

interface ShowUpdateIngredientPresenter
{
    public function present(IngredientItem $ingredient): View;
}
