<?php

namespace App\Presentation\Representation\Admin\Dashboard;

use App\Presentation\Representation\Admin\Dashboard\Contracts\DashboardPresenter as ContractDashboardPresenter;
use Illuminate\Contracts\View\View;

final class DashboardPresenter implements ContractDashboardPresenter
{
    public function present(): View
    {
        return view('web.admin.dashboard.home.index');
    }
}
