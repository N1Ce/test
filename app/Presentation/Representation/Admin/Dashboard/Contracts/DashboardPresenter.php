<?php

namespace App\Presentation\Representation\Admin\Dashboard\Contracts;

use Illuminate\Contracts\View\View;

interface DashboardPresenter
{
    public function present(): View;
}
