<?php

namespace App\Presentation\Providers;

use App\Application\UseCases\Admins\Admin\Contracts\CreateAdminCase as ContractCreateAdminCase;
use App\Application\UseCases\Admins\Admin\CreateAdminCase;
use App\Application\UseCases\Admins\Category\Create\Contracts\CreateCategoryCase as ContractCreateCategoryCase;
use App\Application\UseCases\Admins\Category\Delete\Contracts\DeleteCategoryCase as ContractDeleteCategoryCase;
use App\Application\UseCases\Admins\Category\Show\Contracts\ShowCategoryCase as ContractAdminShowCategoryCase;
use App\Application\UseCases\Admins\Category\Update\Contracts\ShowUpdateCategoryCase as ContractShowUpdateCategoryCase;
use App\Application\UseCases\Admins\Category\Update\Contracts\UpdateCategoryCase as ContractUpdateCategoryCase;
use App\Application\UseCases\Admins\Category\Create\CreateCategoryCase;
use App\Application\UseCases\Admins\Category\Delete\DeleteCategoryCase;
use App\Application\UseCases\Admins\Category\Show\ShowCategoryCase as AdminShowCategoryCase;
use App\Application\UseCases\Admins\Category\Update\ShowUpdateCategoryCase;
use App\Application\UseCases\Admins\Category\Update\UpdateCategoryCase;
use App\Application\UseCases\Admins\Dish\Create\Contracts\CreateDishCase as ContractCreateDishCase;
use App\Application\UseCases\Admins\Dish\Create\Contracts\ShowCreateDishCase as ContractShowCreateDishCase;
use App\Application\UseCases\Admins\Dish\Create\CreateDishCase;
use App\Application\UseCases\Admins\Dish\Create\ShowCreateDishCase;
use App\Application\UseCases\Admins\Dish\Delete\Contracts\DeleteDishCase as ContractDeleteDishCase;
use App\Application\UseCases\Admins\Dish\Delete\DeleteDishCase;
use App\Application\UseCases\Admins\Dish\Show\Contracts\ShowDishCase as ContractAdminShowDishCase;
use App\Application\UseCases\Admins\Dish\Show\ShowDishCase as AdminShowDishCase;
use App\Application\UseCases\Admins\Dish\Update\Contracts\ShowUpdateDishCase as ContractShowUpdateDishCase;
use App\Application\UseCases\Admins\Dish\Update\ShowUpdateDishCase;
use App\Application\UseCases\Admins\Ingredients\Create\Contracts\CreateIngredientCase as ContractCreateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Create\CreateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Delete\Contracts\DeleteIngredientCase as ContractDeleteIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Delete\DeleteIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Show\Contracts\ShowIngredientsCase as ContractShowIngredientsCase;
use App\Application\UseCases\Admins\Ingredients\Show\ShowIngredientsCase;
use App\Application\UseCases\Admins\Ingredients\Update\Contracts\ShowUpdateIngredientCase as ContractShowUpdateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Update\Contracts\UpdateIngredientCase as ContractUpdateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Update\ShowUpdateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Update\UpdateIngredientCase;
use App\Application\UseCases\Clients\Basket\AddBasketCase;
use App\Application\UseCases\Clients\Basket\Contracts\AddBasketCase as ContractAddBasketCase;
use App\Application\UseCases\Clients\Basket\Contracts\RemoveBasketItemCase as ContractRemoveBasketItemCase;
use App\Application\UseCases\Clients\Basket\Contracts\RemoveItemsByIdCase as ContractRemoveItemsByIdCase;
use App\Application\UseCases\Clients\Basket\Contracts\ShowBasketCase as ContractShowBasketCase;
use App\Application\UseCases\Clients\Basket\Contracts\ShowMiniBasketCase as ContractShowMiniBasketCase;
use App\Application\UseCases\Clients\Basket\RemoveBasketItemCase;
use App\Application\UseCases\Clients\Basket\RemoveItemsByIdCase;
use App\Application\UseCases\Clients\Basket\ShowBasketCase;
use App\Application\UseCases\Clients\Basket\ShowMiniBasketCase;
use App\Application\UseCases\Clients\Category\Contracts\ShowCategoryCase as ContractShowCategoryCase;
use App\Application\UseCases\Clients\Category\ShowCategoryCase;
use App\Application\UseCases\Clients\Dish\Contracts\ShowDishCase as ContractShowDishCase;
use App\Application\UseCases\Clients\Dish\ShowDishCase;
use App\Application\UseCases\Clients\Home\Contracts\HomeCase as ContractHomeCase;
use App\Application\UseCases\Clients\HomeCase;
use Illuminate\Support\ServiceProvider;

final class UseCasesProvider extends ServiceProvider
{
    public function register()
    {
        /** Basket UseCase */
        $this->app->bind(ContractAddBasketCase::class, AddBasketCase::class);
        $this->app->bind(ContractRemoveBasketItemCase::class, RemoveBasketItemCase::class);
        $this->app->bind(ContractShowMiniBasketCase::class, ShowMiniBasketCase::class);
        $this->app->bind(ContractShowBasketCase::class, ShowBasketCase::class);
        $this->app->bind(ContractRemoveItemsByIdCase::class, RemoveItemsByIdCase::class);
        /** Category UseCase */
        $this->app->bind(ContractShowCategoryCase::class, ShowCategoryCase::class);
        /** dish UseCase */
        $this->app->bind(ContractShowDishCase::class, ShowDishCase::class);
        /** Home UseCase */
        $this->app->bind(ContractHomeCase::class, HomeCase::class);
        /** Admin UseCase */
        $this->app->bind(ContractCreateAdminCase::class, CreateAdminCase::class);
        $this->app->bind(ContractAdminShowCategoryCase::class, AdminShowCategoryCase::class);
        $this->app->bind(ContractCreateCategoryCase::class, CreateCategoryCase::class);
        $this->app->bind(ContractDeleteCategoryCase::class, DeleteCategoryCase::class);
        $this->app->bind(ContractAdminShowDishCase::class, AdminShowDishCase::class);
        $this->app->bind(ContractShowUpdateCategoryCase::class, ShowUpdateCategoryCase::class);
        $this->app->bind(ContractUpdateCategoryCase::class, UpdateCategoryCase::class);
        $this->app->bind(ContractShowIngredientsCase::class, ShowIngredientsCase::class);
        $this->app->bind(ContractCreateIngredientCase::class, CreateIngredientCase::class);
        $this->app->bind(ContractDeleteIngredientCase::class, DeleteIngredientCase::class);
        $this->app->bind(ContractShowUpdateIngredientCase::class, ShowUpdateIngredientCase::class);
        $this->app->bind(ContractUpdateIngredientCase::class, UpdateIngredientCase::class);
        $this->app->bind(ContractShowCreateDishCase::class, ShowCreateDishCase::class);
        $this->app->bind(ContractCreateDishCase::class, CreateDishCase::class);
        $this->app->bind(ContractDeleteDishCase::class, DeleteDishCase::class);
        $this->app->bind(ContractShowUpdateDishCase::class, ShowUpdateDishCase::class);
    }
}
