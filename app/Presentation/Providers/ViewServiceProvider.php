<?php

namespace App\Presentation\Providers;

use App\Presentation\View\Composers\BasketComposer;
use App\Presentation\View\Composers\CategoryComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.client.includes.menu.left', CategoryComposer::class);
        View::composer('layouts.client.includes.basket', BasketComposer::class);
    }
}
