<?php

namespace App\Presentation\Providers;

use App\Infrastructure\Adapters\Container\Contracts\Container as ContractContainer;
use App\Infrastructure\Adapters\Container\Container;
use App\Infrastructure\Adapters\Storage\Contracts\Storage as ContractStorage;
use App\Infrastructure\Adapters\Storage\Storage;
use Illuminate\Support\ServiceProvider;

class AdaptorsProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /** Adaptor Container */
        $this->app->bind(ContractContainer::class, Container::class);
        /** Adaptor Storage */
        $this->app->bind(ContractStorage::class, Storage::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
