<?php

namespace App\Presentation\Providers;

use App\Application\Service\File\Contracts\FileService as ContractFileService;
use App\Application\Service\File\FileService;
use App\Domain\Basket\Contracts\BasketService;
use App\Domain\Services\Basket\Contracts\BasketStorage;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        /** Basket Services */
        $this->app->bind(
            BasketService::class,
            \App\Domain\Services\Basket\BasketService::class
        );
        $this->app->bind(
            BasketStorage::class,
            \App\Infrastructure\Basket\BasketStorage::class
        );
        /** File Service */
        $this->app->bind(ContractFileService::class, FileService::class);
    }

    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        //
    }
}
