<?php

namespace App\Presentation\Providers;

use App\Domain\Category\Contracts\CategoryRepository as ContractCategoryRepository;
use App\Domain\Images\Contracts\ImagesRepository as ContractImagesRepository;
use App\Domain\Ingredients\Contracts\IngredientsRepository as ContractIngredientsRepository;
use App\Infrastructure\Category\CategoryRepository;
use App\Domain\Dish\Contracts\DishRepository as ContractDishRepository;
use App\Infrastructure\Repositories\Dish\DishRepository;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository as ContractIngredientsItemRepository;
use App\Infrastructure\Repositories\Images\ImagesRepository;
use App\Infrastructure\Repositories\Ingredients\IngredientsRepository;
use App\Infrastructure\Repositories\IngredientsItem\IngredientsItemRepository;
use App\Domain\User\Contracts\UserRepository as ContractUserRepository;
use App\Infrastructure\Repositories\User\UserRepository;
use Illuminate\Support\ServiceProvider;

final class RepositoryProvider extends ServiceProvider
{
    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        /** CategoryRepository */
        $this->app->bind(ContractCategoryRepository::class, CategoryRepository::class);
        /** DishRepository */
        $this->app->bind(ContractDishRepository::class, DishRepository::class);
        /** UserRepository */
        $this->app->bind(ContractUserRepository::class, UserRepository::class);
        /** Ingredients Item Repository */
        $this->app->bind(ContractIngredientsItemRepository::class, IngredientsItemRepository::class);
        /** Ingredients Repository */
        $this->app->bind(ContractIngredientsRepository::class, IngredientsRepository::class);
        /** Images Repository */
        $this->app->bind(ContractImagesRepository::class, ImagesRepository::class);
    }

    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        //
    }
}
