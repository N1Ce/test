<?php

namespace App\Presentation\Providers;

use App\Presentation\Clients\Category\ShowCategoryPresenter;
use App\Presentation\Clients\Home\HomePresenter;
use App\Presentation\Representation\Admin\Category\Contracts\CreateCategoryPresenter as ContractCreateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\Contracts\DeleteCategoryPresenter as ContractDeleteCategoryPresenter;
use App\Presentation\Representation\Admin\Category\Contracts\ShowCategoryPresenter
    as ContractAdminShowCategoryPresenter;
use App\Presentation\Representation\Admin\Category\Contracts\ShowCreateCategoryPresenter
    as ContractShowCreateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\Contracts\ShowUpdateCategoryPresenter
    as ContractShowUpdateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\Contracts\UpdateCategoryPresenter as ContractUpdateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\CreateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\DeleteCategoryPresenter;
use App\Presentation\Representation\Admin\Category\ShowCategoryPresenter as AdminShowCategoryPresenter;
use App\Presentation\Representation\Admin\Category\ShowCreateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\ShowUpdateCategoryPresenter;
use App\Presentation\Representation\Admin\Category\UpdateCategoryPresenter;
use App\Presentation\Representation\Admin\Dashboard\Contracts\DashboardPresenter as ContractDashboardPresenter;
use App\Presentation\Representation\Admin\Dashboard\DashboardPresenter;
use App\Presentation\Representation\Admin\Dish\Contracts\CreateDishPresenter as ContractCreateDishPresenter;
use App\Presentation\Representation\Admin\Dish\Contracts\DeleteDishPresenter as ContractDeleteDishPresenter;
use App\Presentation\Representation\Admin\Dish\Contracts\ShowCreateDishPresenter as ContractShowCreateDishPresenter;
use App\Presentation\Representation\Admin\Dish\Contracts\ShowDishPresenter as ContractAdminShowDishPresenter;
use App\Presentation\Representation\Admin\Dish\CreateDishPresenter;
use App\Presentation\Representation\Admin\Dish\DeleteDishPresenter;
use App\Presentation\Representation\Admin\Dish\ShowCreateDishPresenter;
use App\Presentation\Representation\Admin\Dish\ShowDishPresenter as AdminShowDishPresenter;
use App\Presentation\Representation\Admin\Ingredients\Contracts\CreateIngredientPresenter as ContractCreateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\Contracts\DeleteIngredientPresenter as ContractDeleteIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowCreateIngredientPresenter as ContractShowCreateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\Contracts\ShowUpdateIngredientPresenter as ContractShowUpdateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\Contracts\UpdateIngredientPresenter as ContractUpdateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\CreateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\DeleteIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\ShowCreateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\ShowUpdateIngredientPresenter;
use App\Presentation\Representation\Admin\Ingredients\UpdateIngredientPresenter;
use App\Presentation\Representation\Clients\Basket\AddBasketPresenter;
use App\Presentation\Representation\Clients\Basket\Contracts\AddBasketPresenter as ContractAddBasketPresenter;
use App\Presentation\Representation\Clients\Basket\Contracts\RemoveBasketPresenter as ContractRemoveBasketPresenter;
use App\Presentation\Representation\Clients\Basket\Contracts\ShowBasketPresenter as ContractShowBasketPresenter;
use App\Presentation\Representation\Clients\Basket\RemoveBasketPresenter;
use App\Presentation\Representation\Clients\Basket\ShowBasketPresenter;
use App\Presentation\Representation\Clients\Category\Contracts\ShowCategoryPresenter as ContractShowCategoryPresenter;
use App\Presentation\Representation\Clients\Dish\Contracts\ShowDishPresenter as ContractShowDishPresenter;
use App\Presentation\Representation\Clients\Dish\ShowDishPresenter;
use App\Presentation\Representation\Clients\Home\Contracts\HomePresenter as ContractHomePresenter;
use Illuminate\Support\ServiceProvider;

final class RepresentationProvider extends ServiceProvider
{
    /**
     * Register services.
     * @return void
     */
    public function register()
    {
        $this->app->bind(ContractHomePresenter::class, HomePresenter::class);
        /** Basket presentation */
        $this->app->bind(ContractAddBasketPresenter::class, AddBasketPresenter::class);
        $this->app->bind(ContractRemoveBasketPresenter::class, RemoveBasketPresenter::class);
        $this->app->bind(ContractShowBasketPresenter::class, ShowBasketPresenter::class);
        /** Category presentation */
        $this->app->bind(ContractShowCategoryPresenter::class, ShowCategoryPresenter::class);
        /** dishes presentation */
        $this->app->bind(ContractShowDishPresenter::class, ShowDishPresenter::class);
        /** Admin presentation */
        $this->app->bind(ContractDashboardPresenter::class, DashboardPresenter::class);
        $this->app->bind(ContractShowCreateCategoryPresenter::class, ShowCreateCategoryPresenter::class);
        $this->app->bind(ContractAdminShowCategoryPresenter::class, AdminShowCategoryPresenter::class);
        $this->app->bind(ContractCreateCategoryPresenter::class, CreateCategoryPresenter::class);
        $this->app->bind(ContractDeleteCategoryPresenter::class, DeleteCategoryPresenter::class);
        $this->app->bind(ContractAdminShowDishPresenter::class, AdminShowDishPresenter::class);
        $this->app->bind(ContractShowUpdateCategoryPresenter::class, ShowUpdateCategoryPresenter::class);
        $this->app->bind(ContractUpdateCategoryPresenter::class, UpdateCategoryPresenter::class);
        $this->app->bind(ContractShowCreateIngredientPresenter::class, ShowCreateIngredientPresenter::class);
        $this->app->bind(ContractCreateIngredientPresenter::class, CreateIngredientPresenter::class);
        $this->app->bind(ContractDeleteIngredientPresenter::class, DeleteIngredientPresenter::class);
        $this->app->bind(ContractShowUpdateIngredientPresenter::class, ShowUpdateIngredientPresenter::class);
        $this->app->bind(ContractUpdateIngredientPresenter::class, UpdateIngredientPresenter::class);
        $this->app->bind(ContractShowCreateDishPresenter::class, ShowCreateDishPresenter::class);
        $this->app->bind(ContractCreateDishPresenter::class, CreateDishPresenter::class);
        $this->app->bind(ContractDeleteDishPresenter::class, DeleteDishPresenter::class);
    }

    /**
     * Bootstrap services.
     * @return void
     */
    public function boot()
    {
        //
    }
}
