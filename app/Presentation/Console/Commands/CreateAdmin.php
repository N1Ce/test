<?php

namespace App\Presentation\Console\Commands;

use App\Application\UseCases\Admins\Admin\Contracts\CreateAdminCase;
use App\Application\UseCases\Admins\Admin\Request\CreateAdmin as CreateAdminDTO;
use Illuminate\Console\Command;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'create:admin {email} {password} {name?}';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'create:admin {email} {password} {name?}';

    private CreateAdminCase $case;

    /**
     * CreateAdmin constructor.
     *
     * @param CreateAdminCase $case
     */
    public function __construct(CreateAdminCase $case)
    {
        parent::__construct();
        $this->case = $case;
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        $request = new CreateAdminDTO($this->arguments());
        try {
            $this->case->createAdmin($request);
            $this->info('Admin create!');
        } catch (\Exception $exception) {
            $this->error(sprintf('Cannot create admin. %s', $exception->getMessage()));
        }
    }
}
