<?php

namespace App\Presentation\View\Composers;

use App\Domain\Category\Contracts\CategoryRepository;
use Illuminate\View\View;

class CategoryComposer
{
    /**
     * @var CategoryRepository $categoryService
     */
    protected CategoryRepository $categoryRepository;

    /**
     * CategoryComposer constructor.
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $categories = $this->categoryRepository->getAll();
        $view->with('categories', $categories);
    }
}
