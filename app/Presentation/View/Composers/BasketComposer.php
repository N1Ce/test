<?php

namespace App\Presentation\View\Composers;

use App\Application\UseCases\Clients\Basket\Contracts\ShowMiniBasketCase;
use Illuminate\View\View;

class BasketComposer
{
    private ShowMiniBasketCase $case;

    /**
     * BasketComposer constructor.
     *
     * @param ShowMiniBasketCase $case
     */
    public function __construct(ShowMiniBasketCase $case)
    {
        $this->case = $case;
    }

    /**
     * @param View $view
     */
    public function compose(View $view)
    {
        $basket = $this->case->execute();
        $view->with('basket', $basket);
    }
}
