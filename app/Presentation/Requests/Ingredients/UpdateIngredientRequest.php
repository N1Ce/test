<?php

namespace App\Presentation\Requests\Ingredients;

use Illuminate\Foundation\Http\FormRequest;

class UpdateIngredientRequest extends FormRequest
{
    public const NAME = 'name';
    public const ID = 'id';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::ID => 'required|integer',
            self::NAME => 'required|string|unique:ingredients_item|min:2',
        ];
    }
}
