<?php

namespace App\Presentation\Requests\Ingredients;

use Illuminate\Foundation\Http\FormRequest;

class CreateIngredientRequest extends FormRequest
{
    public const NAME = 'name';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            self::NAME => 'required|string|unique:ingredients_item|min:2',
        ];
    }
}
