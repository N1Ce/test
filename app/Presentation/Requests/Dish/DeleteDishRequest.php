<?php

namespace App\Presentation\Requests\Dish;

use Illuminate\Foundation\Http\FormRequest;

class DeleteDishRequest extends FormRequest
{
    public const ID = 'id';
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            self::ID => 'required|integer|exists:dishes,id',
        ];
    }
}
