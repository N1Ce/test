<?php

namespace App\Presentation\Requests\Dish;

use Illuminate\Foundation\Http\FormRequest;

class CreateDishRequest extends FormRequest
{
    private const NAME = 'name';
    private const CATEGORY = 'category';
    private const PRICE = 'price';
    private const WEIGHT = 'weight';
    private const INGREDIENTS = 'ingredients';
    private const INGREDIENTS_ITEM = 'ingredients.*';
    private const DESCRIPTION = 'description';

    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
            self::NAME => 'required|string|unique:dishes|min:2',
            self::CATEGORY => 'required|integer|exists:categories,id',
            self::PRICE => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/',
            self::WEIGHT => 'required|integer',
            self::INGREDIENTS => 'required|array',
            self::INGREDIENTS_ITEM => 'integer|exists:ingredients_item,id',
            self::DESCRIPTION => 'required|string|min:10'
        ];
    }
}
