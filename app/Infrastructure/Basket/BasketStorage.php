<?php

namespace App\Infrastructure\Basket;

use App\Domain\Basket\Basket;
use App\Domain\Services\Basket\Contracts\BasketStorage as ContractBasketStorage;
use App\Infrastructure\Adapters\Session\SessionAdapter;

class BasketStorage implements ContractBasketStorage
{
    private const BASKET_NAME = 'basket';

    private SessionAdapter $sessionAdapter;

    public function __construct(SessionAdapter $sessionAdapter)
    {
        $this->sessionAdapter = $sessionAdapter;
    }

    /**
     * @return Basket|null
     */
    public function getBasket(): Basket
    {
        return $this->sessionAdapter->get(self::BASKET_NAME) ?: new Basket();
    }

    /**
     * @param Basket $basket
     */
    public function saveBasket(Basket $basket): void
    {
        $this->sessionAdapter
            ->put(self::BASKET_NAME, $basket)
            ->save();
    }
}
