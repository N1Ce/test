<?php

namespace App\Infrastructure\Adapters\Session\Contracts;

use App\Domain\Basket\Basket;

interface SessionAdapter
{
    /**
     * @param string $name
     * @return Basket|null
     */
    public function get(string $name): ?Basket;

    /**
     * @param string $name
     * @param $data
     * @return SessionAdapter
     */
    public function put(string $name, $data): self;

    /**
     * @return SessionAdapter
     */
    public function save(): self;
}
