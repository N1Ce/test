<?php

namespace App\Infrastructure\Adapters\Session;

use App\Domain\Basket\Basket;
use App\Infrastructure\Adapters\Session\Contracts\SessionAdapter as ContractSessionAdapter;
use Illuminate\Contracts\Session\Session;

class SessionAdapter implements ContractSessionAdapter
{
    private Session $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param string $name
     * @return Basket|null
     */
    public function get(string $name): ?Basket
    {
        return $this->session->get($name);
    }

    /**
     * @return SessionAdapter
     */
    public function save(): ContractSessionAdapter
    {
        $this->session->save();

        return $this;
    }

    /**
     * @param string $name
     * @param $data
     * @return ContractSessionAdapter
     */
    public function put(string $name, $data): ContractSessionAdapter
    {
        $this->session->put($name, $data);

        return $this;
    }
}
