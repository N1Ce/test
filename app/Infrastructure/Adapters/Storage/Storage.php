<?php

namespace App\Infrastructure\Adapters\Storage;

use App\Infrastructure\Adapters\Storage\Contracts\Storage as ContractStorage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage as StorageFacades;

class Storage implements ContractStorage
{
    public function save(string $path, UploadedFile $file): string
    {
        StorageFacades::disk('public')->put($path, file_get_contents($file));

        return $path;
    }
}
