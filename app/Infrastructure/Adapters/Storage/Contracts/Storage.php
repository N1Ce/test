<?php

namespace App\Infrastructure\Adapters\Storage\Contracts;

use Illuminate\Http\UploadedFile;

interface Storage
{
    public function save(string $path, UploadedFile $file): string;
}
