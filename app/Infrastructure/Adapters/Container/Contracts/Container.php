<?php

namespace App\Infrastructure\Adapters\Container\Contracts;

interface Container
{
    /**
     * @param string $type
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function make(string $type);
}
