<?php

namespace App\Infrastructure\Adapters\Container;

use App\Infrastructure\Adapters\Container\Contracts\Container as ContractContainer;
use Illuminate\Contracts\Container\Container as IlluminaleContainer;

class Container implements ContractContainer
{
    private IlluminaleContainer $container;

    public function __construct(IlluminaleContainer $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $type
     *
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function make(string $type)
    {
        return $this->container->make($type);
    }
}
