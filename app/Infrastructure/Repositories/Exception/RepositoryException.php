<?php

namespace App\Infrastructure\Repositories\Exception;

use Exception;

class RepositoryException extends Exception
{
}
