<?php

namespace App\Infrastructure\Repositories\User;

use App\Domain\User\Contracts\UserRepository as ContractUserRepository;
use App\Domain\User\User;

class UserRepository implements ContractUserRepository
{
    /**
     * @var User
     */
    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param array $arguments
     *
     * @return User
     */
    public function create(array $arguments): User
    {
        return $this->user->create($arguments);
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public function checkEmail(string $email): bool
    {
        return $this->user->where(User::EMAIL, $email)->exists();
    }
}
