<?php

namespace App\Infrastructure\Category;

use App\Domain\Category\Category;
use App\Domain\Category\Contracts\CategoryRepository as ContractCategoryRepository;
use Illuminate\Database\Eloquent\Collection;

class CategoryRepository implements ContractCategoryRepository
{
    protected Category $category;

    /**
     * CategoryRepository constructor.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->category->all();
    }

    /**
     * @param string $name
     *
     * @return Category
     */
    public function create(string $name): Category
    {
        return $this->category->firstOrCreate([Category::NAME => $name]);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->category->findOrFail($id)->delete();
    }

    /**
     * @param int $id
     *
     * @return Category
     */
    public function getById(int $id): Category
    {
        return $this->category->findOrFail($id);
    }

    /**
     * @param int    $id
     * @param string $name
     *
     * @return Category
     */
    public function update(int $id, string $name): Category
    {
        /** @var Category $category */
        $category = $this->category->findOrFail($id);
        $category->setName($name);
        $category->save();

        return $category;
    }
}
