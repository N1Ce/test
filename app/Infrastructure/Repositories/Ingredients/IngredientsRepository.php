<?php

namespace App\Infrastructure\Repositories\Ingredients;

use App\Domain\Ingredients\Contracts\IngredientsRepository as ContractIngredientsRepository;
use App\Domain\Ingredients\Ingredients;

class IngredientsRepository implements ContractIngredientsRepository
{
    private Ingredients $ingredients;

    public function __construct(Ingredients $ingredients)
    {
        $this->ingredients = $ingredients;
    }

    public function create(int $dishID, int $ingredientID): Ingredients
    {
        return $this->ingredients->create([
            Ingredients::DISH_ID => $dishID,
            Ingredients::INGREDIENTS_ITEM_ID => $ingredientID
        ]);
    }
}
