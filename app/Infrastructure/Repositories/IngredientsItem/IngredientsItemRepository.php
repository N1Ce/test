<?php

namespace App\Infrastructure\Repositories\IngredientsItem;

use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository as ContractIngredientsItemRepository;
use App\Domain\IngredientsItem\IngredientItem;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class IngredientsItemRepository implements ContractIngredientsItemRepository
{
    /** @var IngredientItem $item */
    private IngredientItem $item;

    public function __construct(IngredientItem $item)
    {
        $this->item = $item;
    }

    /**
     * @param int $count
     *
     * @return LengthAwarePaginator
     */
    public function getAllWithPagination(int $count = 10): LengthAwarePaginator
    {
        return $this->item->paginate($count);
    }

    public function getAll(): Collection
    {
        return $this->item->all();
    }

    /**
     * @param string $name
     *
     * @return IngredientItem
     */
    public function create(string $name): IngredientItem
    {
        return $this->item->firstOrCreate([IngredientItem::NAME => $name]);
    }

    /**
     * @param int $id
     *
     * @return bool
     */
    public function delete(int $id): bool
    {
        return $this->item->findOrFail($id)->delete();
    }

    /**
     * @param int $id
     *
     * @return IngredientItem
     */
    public function getByID(int $id): IngredientItem
    {
        return $this->item->findOrFail($id);
    }

    /**
     * @param int    $id
     * @param string $name
     *
     * @return IngredientItem
     */
    public function update(int $id, string $name): IngredientItem
    {
        $item = $this->getByID($id);
        $item->update([
            IngredientItem::NAME => $name
        ]);
        $item->save();

        return $item;
    }
}
