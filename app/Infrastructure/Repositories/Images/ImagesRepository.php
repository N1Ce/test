<?php

namespace App\Infrastructure\Repositories\Images;

use App\Domain\Images\Contracts\ImagesRepository as ContractImagesRepository;
use App\Domain\Images\Images;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ImagesRepository implements ContractImagesRepository
{
    private Images $images;

    public function __construct(Images $images)
    {
        $this->images = $images;
    }

    public function associate(string $path, Model $model): Model
    {
        $image = $this->images->create(
            [
                Images::PATH => $path,
                Images::IMAGEABLE_ID => $model->getKey(),
                Images::IMAGEABLE_TYPE => get_class($model),
            ]
        );

        return $image;
    }

    /**
     * @param int    $id
     * @param string $model
     */
    public function delete(int $id, string $model): void
    {
        $images = $this->images->where([Images::IMAGEABLE_ID => $id, Images::IMAGEABLE_TYPE => $model])->get();
        foreach ($images as $image) {
            $array = explode('/', $image->getPath());
            unset($array[array_key_first($array)]);
            $path = implode('/', $array);
            Storage::disk('public')->delete($path);
        }
    }
}
