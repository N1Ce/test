<?php

namespace App\Infrastructure\Repositories\Dish;

use App\Domain\Dish\Contracts\DishRepository as ContractDishRepository;
use App\Domain\Dish\Dish;
use App\Domain\Dish\Request\Create;
use App\Infrastructure\Repositories\Exception\RepositoryException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Exception;

class DishRepository implements ContractDishRepository
{
    private Dish $dish;

    public function __construct(Dish $dish)
    {
        $this->dish = $dish;
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getDishes(): LengthAwarePaginator
    {
        return $this->dish->with([Dish::IMAGE, Dish::CATEGORY])->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return LengthAwarePaginator
     */
    public function getDishesByCategoryID(int $id): LengthAwarePaginator
    {
        return $this->dish->where(Dish::CATEGORY_ID, $id)->with(Dish::IMAGE)->paginate(10);
    }

    /**
     * @param int $id
     *
     * @return Dish|null
     */
    public function getDishByIdAndImages(int $id): ?Dish
    {
        return $this->dish->with(Dish::IMAGES)->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return Dish|null
     */
    public function getDishById(int $id): ?Dish
    {
        return $this->dish->findOrFail($id);
    }

    /**
     * @param int $id
     *
     * @return Dish
     */
    public function getDishWithImage(int $id): Dish
    {
        return $this->dish->with(Dish::IMAGE)->findOrFail($id);
    }

    /**
     * @param array $array
     *
     * @return Collection
     */
    public function getDishesByArray(array $array): Collection
    {
        return $this->dish->whereIn(Dish::ID, $array)->with(Dish::IMAGE)->get();
    }

    /**
     * @param Create $create
     *
     * @return Dish
     */
    public function create(Create $create): Dish
    {
        return $this->dish->create($create->toArrayByDish());
    }

    /**
     * @param int $id
     *
     * @return Dish|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model
     * @throws RepositoryException
     */
    public function getDishByIdWhitImageAndIngredients(int $id): Dish
    {
        try {
            return $this->dish->with([Dish::IMAGE, Dish::INGREDIENTS])->findOrFail($id);
        } catch (Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }
Всем привет! Сегодня мой последний день в Light :lightit: :cry:
Спасибо всем, кто помогал развиваться!
Успехов компании и каждому сотруднику в частности!!!
    /**
     * @param int $id
     *
     * @return bool
     * @throws RepositoryException
     */
    public function delete(int $id): bool
    {
        try {
            /** @var Model $dish */
            $dish = $this->dish->findOrFail($id);

            return $dish->delete();
        } catch (Exception $exception) {
            throw new RepositoryException($exception->getMessage());
        }
    }
}
