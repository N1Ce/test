<?php

namespace App\Application\UseCases\Admins\Dish\Exception;

use Exception;

class DishException extends Exception
{
}
