<?php

namespace App\Application\UseCases\Admins\Dish\Delete\Contracts;

use App\Application\UseCases\Admins\Dish\Exception\DishException;

interface DeleteDishCase
{
    /**
     * @param int $id
     *
     * @throws DishException
     */
    public function execute(int $id): void;
}
