<?php

namespace App\Application\UseCases\Admins\Dish\Delete;

use App\Application\UseCases\Admins\Dish\Delete\Contracts\DeleteDishCase as ContractDeleteDishCase;
use App\Application\UseCases\Admins\Dish\Exception\DishException;
use App\Domain\Dish\Contracts\DishRepository;
use App\Domain\Dish\Dish;
use App\Domain\Images\Contracts\ImagesRepository;
use App\Infrastructure\Repositories\Exception\RepositoryException;

class DeleteDishCase implements ContractDeleteDishCase
{
    private DishRepository $dishRepository;

    private ImagesRepository $imagesRepository;

    public function __construct(DishRepository $dishRepository, ImagesRepository $imagesRepository)
    {
        $this->dishRepository = $dishRepository;
        $this->imagesRepository = $imagesRepository;
    }

    /**
     * @param int $id
     *
     * @throws DishException
     */
    public function execute(int $id): void
    {
        try {
            if ($this->dishRepository->delete($id)) {
                $this->imagesRepository->delete($id, Dish::class);
            }
        } catch (RepositoryException $exception) {
            throw new DishException($exception->getMessage());
        }
    }
}
