<?php

namespace App\Application\UseCases\Admins\Dish\Show\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface ShowDishCase
{
    public function getDishes(): LengthAwarePaginator;
}
