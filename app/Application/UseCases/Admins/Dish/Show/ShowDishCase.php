<?php

namespace App\Application\UseCases\Admins\Dish\Show;

use App\Application\UseCases\Admins\Dish\Show\Contracts\ShowDishCase as ContractShowDishCase;
use App\Domain\Dish\Contracts\DishRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ShowDishCase implements ContractShowDishCase
{
    private DishRepository $dishRepository;

    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    public function getDishes(): LengthAwarePaginator
    {
        return $this->dishRepository->getDishes();
    }
}
