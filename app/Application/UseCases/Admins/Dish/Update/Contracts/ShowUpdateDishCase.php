<?php

namespace App\Application\UseCases\Admins\Dish\Update\Contracts;

use App\Domain\Dish\Dish;

interface ShowUpdateDishCase
{
    public function execute(int $id): Dish;
}
