<?php

namespace App\Application\UseCases\Admins\Dish\Update;

use App\Application\UseCases\Admins\Dish\Update\Contracts\ShowUpdateDishCase as ContractShowUpdateDishCase;
use App\Domain\Dish\Contracts\DishRepository;
use App\Domain\Dish\Dish;

class ShowUpdateDishCase implements ContractShowUpdateDishCase
{
    private DishRepository $dishRepository;

    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    public function execute(int $id): Dish
    {
        return $this->dishRepository->getDishByIdWhitImageAndIngredients($id);
    }
}
