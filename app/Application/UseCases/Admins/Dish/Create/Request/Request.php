<?php

namespace App\Application\UseCases\Admins\Dish\Create\Request;

use Illuminate\Http\UploadedFile;

class Request
{
    private const NAME = 'name';
    private const CATEGORY = 'category';
    private const PRICE = 'price';
    private const WEIGHT = 'weight';
    private const INGREDIENTS = 'ingredients';
    private const DESCRIPTION = 'description';

    /** @var string $name */
    private string $name;

    /** @var int $categoryID */
    private int $categoryID;

    /** @var float $price */
    private float $price;

    /** @var int $weight */
    private int $weight;

    /** @var array<int> $ingredients */
    private array $ingredients;

    /** @var string $description */
    private string $description;

    /** @var UploadedFile|null $file */
    private UploadedFile $file;

    /**
     * Request constructor.
     *
     * @param array        $array
     * @param UploadedFile $file
     */
    public function __construct(array $array, UploadedFile $file = null)
    {
        $this->name = data_get($array, self::NAME);
        $this->categoryID = (int)data_get($array, self::CATEGORY);
        $this->price = (float)data_get($array, self::PRICE);
        $this->weight = (int)data_get($array, self::WEIGHT);
        $this->ingredients = data_get($array, self::INGREDIENTS);
        $this->description = data_get($array, self::DESCRIPTION);
        $this->file = $file;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file ?? null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCategoryID(): int
    {
        return $this->categoryID;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @return array
     */
    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    public function toArray(): array
    {
        return [
            self::NAME => $this->getName(),
            self::CATEGORY => $this->getCategoryID(),
            self::PRICE => $this->getPrice(),
            self::WEIGHT => $this->getWeight(),
            self::INGREDIENTS => $this->getIngredients(),
            self::DESCRIPTION => $this->getDescription(),
        ];
    }
}
