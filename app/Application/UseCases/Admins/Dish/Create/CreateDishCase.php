<?php

namespace App\Application\UseCases\Admins\Dish\Create;

use App\Application\Service\File\Actions\DishAction;
use App\Application\Service\File\Contracts\FileService;
use App\Application\UseCases\Admins\Dish\Create\Contracts\CreateDishCase as ContractCreateDishCase;
use App\Application\UseCases\Admins\Dish\Create\Request\Request;
use App\Domain\Dish\Contracts\DishRepository;
use App\Domain\Dish\Dish;
use App\Domain\Dish\Request\Create;
use App\Domain\Images\Contracts\ImagesRepository;
use App\Domain\Ingredients\Contracts\IngredientsRepository;

class CreateDishCase implements ContractCreateDishCase
{
    private const STORAGE = 'storage';

    private DishRepository $dishRepository;

    private IngredientsRepository $ingredientsRepository;

    private FileService $fileService;

    private ImagesRepository $imagesRepository;

    public function __construct(
        DishRepository $dishRepository,
        IngredientsRepository $ingredientsRepository,
        FileService $fileService,
        ImagesRepository $repository
    ) {
        $this->dishRepository = $dishRepository;
        $this->ingredientsRepository = $ingredientsRepository;
        $this->fileService = $fileService;
        $this->imagesRepository = $repository;
    }

    public function execute(Request $request): Dish
    {
        $dish = $this->createDish($request);
        $this->createIngredients($request, $dish);
        $imagesPath = $this->fileService->buildAction(DishAction::class)->save($request->getFile());
        $this->associateFileWithDish($imagesPath, $dish);

        return $dish;
    }

    /**
     * This function create dishes and return Model
     *
     * @param Request $request
     *
     * @return Dish
     */
    private function createDish(Request $request): Dish
    {
        $createDTO = new Create($request->toArray());

        return $this->dishRepository->create($createDTO);
    }

    /**
     * This function connects dishes id and ingredient array id
     *
     * @param Request $request
     * @param Dish    $dish
     */
    private function createIngredients(Request $request, Dish $dish): void
    {
        foreach ($request->getIngredients() as $ingredientID) {
            $this->ingredientsRepository->create($dish->getID(), $ingredientID);
        }
    }

    /**
     * @param string $path
     * @param Dish   $dish
     */
    private function associateFileWithDish(string $path, Dish $dish): void
    {
        $path = self::STORAGE.'/'.$path;
        $this->imagesRepository->associate($path, $dish);
    }
}
