<?php

namespace App\Application\UseCases\Admins\Dish\Create\Response;

use Illuminate\Database\Eloquent\Collection;

class Response
{
    /** @var Collection $categories */
    private Collection $categories;

    /** @var Collection $ingredients */
    private Collection $ingredients;

    public function __construct(Collection $categories, Collection $ingredients)
    {
        $this->categories = $categories;
        $this->ingredients = $ingredients;
    }

    /**
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * @return Collection
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }
}
