<?php

namespace App\Application\UseCases\Admins\Dish\Create;

use App\Application\UseCases\Admins\Dish\Create\Contracts\ShowCreateDishCase as ContractShowCreateDishCase;
use App\Application\UseCases\Admins\Dish\Create\Response\Response;
use App\Domain\Category\Contracts\CategoryRepository;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository;

class ShowCreateDishCase implements ContractShowCreateDishCase
{
    private CategoryRepository $category;

    private IngredientsItemRepository $ingredient;

    public function __construct(CategoryRepository $category, IngredientsItemRepository $ingredient)
    {
        $this->category = $category;
        $this->ingredient = $ingredient;
    }

    public function handler(): Response
    {
        $categories = $this->category->getAll();
        $ingredients = $this->ingredient->getAll();

        return new Response($categories, $ingredients);
    }
}
