<?php

namespace App\Application\UseCases\Admins\Dish\Create\Contracts;

use App\Application\UseCases\Admins\Dish\Create\Response\Response;

interface ShowCreateDishCase
{
    public function handler(): Response;
}
