<?php

namespace App\Application\UseCases\Admins\Dish\Create\Contracts;

use App\Application\UseCases\Admins\Dish\Create\Request\Request;
use App\Domain\Dish\Dish;

interface CreateDishCase
{
    public function execute(Request $request): Dish;
}
