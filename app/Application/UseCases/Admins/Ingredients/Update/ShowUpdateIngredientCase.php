<?php

namespace App\Application\UseCases\Admins\Ingredients\Update;

use App\Application\UseCases\Admins\Ingredients\Update\Contracts\ShowUpdateIngredientCase as ContractShowUpdateIngredientCase;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository;
use App\Domain\IngredientsItem\IngredientItem;

final class ShowUpdateIngredientCase implements ContractShowUpdateIngredientCase
{
    private IngredientsItemRepository $repository;

    public function __construct(IngredientsItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getIngredient(int $id): IngredientItem
    {
        return $this->repository->getByID($id);
    }
}
