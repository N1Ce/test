<?php

namespace App\Application\UseCases\Admins\Ingredients\Update\Request;

final class Update
{
    private const NAME = 'name';
    private const ID = 'id';

    /** @var string $name */
    private string $name;

    /** @var int $id */
    private int $id;

    public function __construct(array $array)
    {
        $this->id = data_get($array, self::ID);
        $this->name = data_get($array, self::NAME);
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
