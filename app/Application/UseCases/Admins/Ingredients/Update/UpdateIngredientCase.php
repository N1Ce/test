<?php

namespace App\Application\UseCases\Admins\Ingredients\Update;

use App\Application\UseCases\Admins\Ingredients\Update\Contracts\UpdateIngredientCase as ContractUpdateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Update\Request\Update;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository;
use App\Domain\IngredientsItem\IngredientItem;

final class UpdateIngredientCase implements ContractUpdateIngredientCase
{
    private IngredientsItemRepository $repository;

    public function __construct(IngredientsItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function updateIngredients(Update $update): IngredientItem
    {
        return $this->repository->update($update->getID(), $update->getName());
    }
}
