<?php

namespace App\Application\UseCases\Admins\Ingredients\Update\Contracts;

use App\Domain\IngredientsItem\IngredientItem;

interface ShowUpdateIngredientCase
{
    public function getIngredient(int $id): IngredientItem;
}
