<?php

namespace App\Application\UseCases\Admins\Ingredients\Update\Contracts;

use App\Application\UseCases\Admins\Ingredients\Update\Request\Update;
use App\Domain\IngredientsItem\IngredientItem;

interface UpdateIngredientCase
{
    public function updateIngredients(Update $update): IngredientItem;
}
