<?php

namespace App\Application\UseCases\Admins\Ingredients\Create\Contracts;

use App\Application\UseCases\Admins\Ingredients\Create\Request\Ingredient;
use App\Domain\IngredientsItem\IngredientItem;

interface CreateIngredientCase
{
    public function createIngredient(Ingredient $ingredient): IngredientItem;
}
