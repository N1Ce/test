<?php

namespace App\Application\UseCases\Admins\Ingredients\Create\Request;

final class Ingredient
{
    private const NAME = 'name';

    private string $name;

    public function __construct(array $array)
    {
        $this->name = data_get($array, self::NAME);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
