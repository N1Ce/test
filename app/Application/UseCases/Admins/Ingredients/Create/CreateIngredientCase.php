<?php

namespace App\Application\UseCases\Admins\Ingredients\Create;

use App\Application\UseCases\Admins\Ingredients\Create\Contracts\CreateIngredientCase as ContractCreateIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Create\Request\Ingredient;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository;
use App\Domain\IngredientsItem\IngredientItem;

final class CreateIngredientCase implements ContractCreateIngredientCase
{
    private IngredientsItemRepository $repository;

    public function __construct(IngredientsItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createIngredient(Ingredient $ingredient): IngredientItem
    {
        return $this->repository->create($ingredient->getName());
    }
}
