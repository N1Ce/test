<?php

namespace App\Application\UseCases\Admins\Ingredients\Delete\Contracts;

use App\Application\UseCases\Admins\Ingredients\Delete\Request\Delete;

interface DeleteIngredientCase
{
    public function deleteIngredient(Delete $ingredient): bool;
}
