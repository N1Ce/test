<?php

namespace App\Application\UseCases\Admins\Ingredients\Delete\Request;

final class Delete
{
    private const ID = 'id';

    private int $id;

    public function __construct(array $array)
    {
        $this->id = data_get($array, self::ID);
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->id;
    }
}
