<?php

namespace App\Application\UseCases\Admins\Ingredients\Delete;

use App\Application\UseCases\Admins\Ingredients\Delete\Contracts\DeleteIngredientCase as ContractDeleteIngredientCase;
use App\Application\UseCases\Admins\Ingredients\Delete\Request\Delete;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository;

final class DeleteIngredientCase implements ContractDeleteIngredientCase
{
    private IngredientsItemRepository $repository;

    public function __construct(IngredientsItemRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Delete $ingredient
     *
     * @return bool
     */
    public function deleteIngredient(Delete $ingredient): bool
    {
        return $this->repository->delete($ingredient->getID());
    }
}
