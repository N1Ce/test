<?php

namespace App\Application\UseCases\Admins\Ingredients\Show;

use App\Application\UseCases\Admins\Ingredients\Show\Contracts\ShowIngredientsCase as ContractShowIngredientsCase;
use App\Domain\IngredientsItem\Contracts\IngredientsItemRepository;
use Illuminate\Pagination\LengthAwarePaginator;

final class ShowIngredientsCase implements ContractShowIngredientsCase
{
    private IngredientsItemRepository $repository;

    public function __construct(IngredientsItemRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getIngredients(): LengthAwarePaginator
    {
        return $this->repository->getAllWithPagination();
    }
}
