<?php

namespace App\Application\UseCases\Admins\Ingredients\Show\Contracts;

use Illuminate\Pagination\LengthAwarePaginator;

interface ShowIngredientsCase
{
    public function getIngredients(): LengthAwarePaginator;
}
