<?php

namespace App\Application\UseCases\Admins\Category\Create\Contracts;

use App\Domain\Category\Category;
use App\Application\UseCases\Admins\Category\Create\Request\CreateCategory;

interface CreateCategoryCase
{
    /**
     * @param CreateCategory $request
     *
     * @return Category
     */
    public function createCategory(CreateCategory $request): Category;
}
