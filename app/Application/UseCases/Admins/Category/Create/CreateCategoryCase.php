<?php

namespace App\Application\UseCases\Admins\Category\Create;

use App\Application\UseCases\Admins\Category\Create\Contracts\CreateCategoryCase as ContractCreateCategoryCase;
use App\Application\UseCases\Admins\Category\Create\Request\CreateCategory;
use App\Domain\Category\Category;
use App\Domain\Category\Contracts\CategoryRepository;

final class CreateCategoryCase implements ContractCreateCategoryCase
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @param CreateCategory $request
     *
     * @return Category
     */
    public function createCategory(CreateCategory $request): Category
    {
        return $this->categoryRepository->create($request->getName());
    }
}
