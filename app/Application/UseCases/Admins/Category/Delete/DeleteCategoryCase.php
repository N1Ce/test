<?php

namespace App\Application\UseCases\Admins\Category\Delete;

use App\Application\UseCases\Admins\Category\Delete\Contracts\DeleteCategoryCase as ContractDeleteCategoryCase;
use App\Application\UseCases\Admins\Category\Delete\Request\DeleteCategory;
use App\Domain\Category\Contracts\CategoryRepository;

final class DeleteCategoryCase implements ContractDeleteCategoryCase
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function deleteCategory(DeleteCategory $request): void
    {
        $this->categoryRepository->delete($request->getID());
    }
}
