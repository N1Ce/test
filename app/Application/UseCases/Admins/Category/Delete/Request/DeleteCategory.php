<?php

namespace App\Application\UseCases\Admins\Category\Delete\Request;

final class DeleteCategory
{
    private const ID = 'id';

    /** @var int $id */
    private int $id;

    public function __construct(array $array)
    {
        $this->id = data_get($array, self::ID);
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->id;
    }
}
