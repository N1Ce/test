<?php

namespace App\Application\UseCases\Admins\Category\Delete\Contracts;

use App\Application\UseCases\Admins\Category\Delete\Request\DeleteCategory;

interface DeleteCategoryCase
{
    public function deleteCategory(DeleteCategory $request): void;
}
