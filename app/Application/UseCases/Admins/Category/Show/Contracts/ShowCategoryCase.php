<?php

namespace App\Application\UseCases\Admins\Category\Show\Contracts;

use Illuminate\Support\Collection;

interface ShowCategoryCase
{
    /**
     * @return Collection
     */
    public function getCategories(): Collection;
}
