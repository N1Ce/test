<?php

namespace App\Application\UseCases\Admins\Category\Show;

use App\Application\UseCases\Admins\Category\Show\Contracts\ShowCategoryCase as ContractShowCategoryCase;
use App\Domain\Category\Contracts\CategoryRepository;
use Illuminate\Support\Collection;

final class ShowCategoryCase implements ContractShowCategoryCase
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return Collection
     */
    public function getCategories(): Collection
    {
        return $this->categoryRepository->getAll();
    }
}
