<?php

namespace App\Application\UseCases\Admins\Category\Update;

use App\Application\UseCases\Admins\Category\Update\Contracts\UpdateCategoryCase as ContractUpdateCategoryCase;
use App\Application\UseCases\Admins\Category\Update\Request\UpdateCategory;
use App\Domain\Category\Category;
use App\Domain\Category\Contracts\CategoryRepository;

final class UpdateCategoryCase implements ContractUpdateCategoryCase
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function updateCategory(UpdateCategory $request): Category
    {
        return $this->categoryRepository->update($request->getID(), $request->getName());
    }
}
