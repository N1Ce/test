<?php

namespace App\Application\UseCases\Admins\Category\Update;

use App\Application\UseCases\Admins\Category\Update\Contracts\ShowUpdateCategoryCase as ContractShowUpdateCategoryCase;
use App\Domain\Category\Category;
use App\Domain\Category\Contracts\CategoryRepository;

final class ShowUpdateCategoryCase implements ContractShowUpdateCategoryCase
{
    private CategoryRepository $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function getCategory(int $id): Category
    {
        return $this->categoryRepository->getById($id);
    }
}
