<?php

namespace App\Application\UseCases\Admins\Category\Update\Contracts;

use App\Domain\Category\Category;

interface ShowUpdateCategoryCase
{
    public function getCategory(int $id): Category;
}
