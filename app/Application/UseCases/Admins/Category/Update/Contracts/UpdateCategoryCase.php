<?php

namespace App\Application\UseCases\Admins\Category\Update\Contracts;

use App\Application\UseCases\Admins\Category\Update\Request\UpdateCategory;
use App\Domain\Category\Category;

interface UpdateCategoryCase
{
    public function updateCategory(UpdateCategory $request): Category;
}
