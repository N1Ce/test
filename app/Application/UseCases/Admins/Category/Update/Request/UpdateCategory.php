<?php

namespace App\Application\UseCases\Admins\Category\Update\Request;

final class UpdateCategory
{
    private const NAME = 'name';
    private const ID = 'id';

    /** @var int $id */
    private int $id;

    /** @var string $name */
    private string $name;

    public function __construct(array $array)
    {
        $this->id = (int)data_get($array, self::ID);
        $this->name = data_get($array, self::NAME);
    }

    /**
     * @return int
     */
    public function getID(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
