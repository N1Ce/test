<?php

namespace App\Application\UseCases\Admins\Admin\Exception;

use Exception;

final class AdminException extends Exception
{
}
