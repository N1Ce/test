<?php

namespace App\Application\UseCases\Admins\Admin\Request;

final class CreateAdmin
{
    private const EMAIL = 'email';
    private const PASSWORD = 'password';
    private const NAME = 'name';

    /**
     * @var string
     */
    private string $email;

    /**
     * @var string
     */
    private string $password;

    /**
     * @var string
     */
    private string $name;

    public function __construct(array $array)
    {
        $this->email = data_get($array, self::EMAIL);
        $this->password = data_get($array, self::PASSWORD);
        $this->name = data_get($array, self::NAME, 'admin');
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            self::EMAIL => $this->getEmail(),
            self::PASSWORD => $this->getPassword(),
            self::NAME => $this->getName()
        ];
    }
}
