<?php

namespace App\Application\UseCases\Admins\Admin;

use App\Application\UseCases\Admins\Admin\Contracts\CreateAdminCase as ContractCreateAdminCase;
use App\Application\UseCases\Admins\Admin\Exception\AdminException;
use App\Application\UseCases\Admins\Admin\Request\CreateAdmin;
use App\Domain\User\Contracts\UserRepository;
use App\Domain\User\User;

final class CreateAdminCase implements ContractCreateAdminCase
{
    private UserRepository $userRepository;

    public function __construct(UserRepository $service)
    {
        $this->userRepository = $service;
    }

    /**
     * @param CreateAdmin $request
     *
     * @return User
     * @throws AdminException
     */
    public function createAdmin(CreateAdmin $request): User
    {
        if ($this->userRepository->checkEmail($request->getEmail())) {
            throw new AdminException('Email already exists!');
        }

        return $this->userRepository->create($request->toArray());
    }
}
