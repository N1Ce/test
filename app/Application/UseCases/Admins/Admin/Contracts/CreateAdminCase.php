<?php

namespace App\Application\UseCases\Admins\Admin\Contracts;

use App\Application\UseCases\Admins\Admin\Exception\AdminException;
use App\Application\UseCases\Admins\Admin\Request\CreateAdmin;
use App\Domain\User\User;

interface CreateAdminCase
{
    /**
     * @param CreateAdmin $arguments
     *
     * @return User
     * @throws AdminException
     */

    public function createAdmin(CreateAdmin $arguments): User;
}
