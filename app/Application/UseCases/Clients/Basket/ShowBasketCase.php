<?php

namespace App\Application\UseCases\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\ShowBasketCase as ContractShowBasketCase;
use App\Domain\Basket\Contracts\BasketService;
use App\Domain\Basket\Item;
use App\Domain\Dish\Contracts\DishRepository;
use Illuminate\Support\Collection;

final class ShowBasketCase implements ContractShowBasketCase
{
    private BasketService $basketService;

    private DishRepository $dishRepository;

    public function __construct(BasketService $basketService, DishRepository $dishRepository)
    {
        $this->basketService = $basketService;
        $this->dishRepository = $dishRepository;
    }

    public function execute(): Collection
    {
        $basket = $this->basketService->getBasket();
        $itemsID = $this->getArrayId($basket->getItems());
        /** @var Collection $dishes */
        $dishes = $this->dishRepository->getDishesByArray($itemsID);

        return $this->getCollectionBasketItems($dishes, $itemsID);
    }

    /**
     * @param array $items
     *
     * @return array
     */
    private function getArrayId(array $items): array
    {
        return array_map(
            function (Item $item) {
                return $item->getProductID();
            },
            $items
        );
    }

    /**
     * @param $dishes
     * @param array      $itemsID
     *
     * @return Collection
     */
    private function getCollectionBasketItems($dishes, array $itemsID): Collection
    {
        $collect = collect();

        foreach ($itemsID as $id) {
            $collect->push($dishes->whereIn('id', $id)->first());
        }

        return $collect;
    }
}
