<?php

namespace App\Application\UseCases\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\ShowMiniBasketCase as ContractShowMiniBasketCase;
use App\Domain\Basket\Basket;
use App\Domain\Basket\Contracts\BasketService;

final class ShowMiniBasketCase implements ContractShowMiniBasketCase
{
    private BasketService $basketService;

    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    public function execute(): Basket
    {
        return $this->basketService->getBasket();
    }
}
