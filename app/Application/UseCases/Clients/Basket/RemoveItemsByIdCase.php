<?php

namespace App\Application\UseCases\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\RemoveItemsByIdCase as ContractsRemoveItemsByIdCase;
use App\Domain\Basket\Contracts\BasketService;

final class RemoveItemsByIdCase implements ContractsRemoveItemsByIdCase
{
    private BasketService $basketService;

    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    public function execute(int $id): void
    {
        $basket = $this->basketService->getBasket();
        $basket->removeItems($id);
        $this->basketService->saveBasket($basket);
    }
}
