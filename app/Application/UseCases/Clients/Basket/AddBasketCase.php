<?php

namespace App\Application\UseCases\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\AddBasketCase as ContractAddBasketCase;
use App\Domain\Basket\Contracts\BasketService;
use App\Domain\Dish\Contracts\DishRepository;

final class AddBasketCase implements ContractAddBasketCase
{
    private BasketService $basketService;

    private DishRepository $dishRepository;

    public function __construct(BasketService $basketService, DishRepository $dishRepository)
    {
        $this->basketService = $basketService;
        $this->dishRepository = $dishRepository;
    }

    public function execute(int $id): void
    {
        $dish = $this->dishRepository->getDishById($id);
        $this->basketService->addBasket($dish);
    }
}
