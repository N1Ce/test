<?php

namespace App\Application\UseCases\Clients\Basket;

use App\Application\UseCases\Clients\Basket\Contracts\RemoveBasketItemCase as ContractRemoveBasketItemCase;
use App\Domain\Basket\Contracts\BasketService;
use App\Domain\Dish\Contracts\DishRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;

final class RemoveBasketItemCase implements ContractRemoveBasketItemCase
{
    private BasketService $basketService;

    private DishRepository $dishRepository;

    public function __construct(BasketService $basketService, DishRepository $dishRepository)
    {
        $this->basketService = $basketService;
        $this->dishRepository = $dishRepository;
    }

    public function execute(int $id): void
    {
        $dish = $this->dishRepository->getDishById($id);

        if (! $dish) {
            throw new ModelNotFoundException();
        }

        $this->basketService->removeItem($dish);
    }
}
