<?php

namespace App\Application\UseCases\Clients\Basket\Contracts;

use Illuminate\Support\Collection;

interface ShowBasketCase
{
    public function execute(): Collection;
}
