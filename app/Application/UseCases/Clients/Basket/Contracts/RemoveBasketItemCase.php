<?php

namespace App\Application\UseCases\Clients\Basket\Contracts;

interface RemoveBasketItemCase
{
    /**
     * @param int $id
     */
    public function execute(int $id): void;
}
