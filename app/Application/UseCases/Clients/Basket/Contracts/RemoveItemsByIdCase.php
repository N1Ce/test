<?php

namespace App\Application\UseCases\Clients\Basket\Contracts;

interface RemoveItemsByIdCase
{
    public function execute(int $id): void;
}
