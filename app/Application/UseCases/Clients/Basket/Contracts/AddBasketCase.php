<?php

namespace App\Application\UseCases\Clients\Basket\Contracts;

/**
 * Interface AddBasketCase
 * @package App\Application\UseCases\Clients\Basket\Contracts
 */
interface AddBasketCase
{
    /**
     * @param int $id
     */
    public function execute(int $id): void;
}
