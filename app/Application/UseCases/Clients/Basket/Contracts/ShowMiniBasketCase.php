<?php

namespace App\Application\UseCases\Clients\Basket\Contracts;

use App\Domain\Basket\Basket;

interface ShowMiniBasketCase
{
    public function execute(): Basket;
}
