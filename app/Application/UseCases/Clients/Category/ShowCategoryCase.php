<?php

namespace App\Application\UseCases\Clients\Category;

use App\Application\UseCases\Clients\Category\Contracts\ShowCategoryCase as ContractShowCategoryCase;
use App\Domain\Dish\Contracts\DishRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

final class ShowCategoryCase implements ContractShowCategoryCase
{
    private DishRepository $dishRepository;

    /**
     * ShowCategoryCase constructor.
     * @param DishRepository $dishRepository
     */
    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    public function execute(int $id): LengthAwarePaginator
    {
        $dishes = $this->dishRepository->getDishesByCategoryID($id);

        return $dishes;
    }
}
