<?php

namespace App\Application\UseCases\Clients\Category\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface ShowCategoryCase
{
    public function execute(int $id): LengthAwarePaginator;
}