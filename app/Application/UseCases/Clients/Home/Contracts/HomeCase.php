<?php

namespace App\Application\UseCases\Clients\Home\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface HomeCase
{
    public function execute(): LengthAwarePaginator;
}