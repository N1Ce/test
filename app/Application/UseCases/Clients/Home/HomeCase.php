<?php

namespace App\Application\UseCases\Clients;

use App\Application\UseCases\Clients\Home\Contracts\HomeCase as ContractHomeCase;
use App\Domain\Dish\Contracts\DishRepository;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

final class HomeCase implements ContractHomeCase
{
    private DishRepository $dishRepository;

    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    public function execute(): LengthAwarePaginator
    {
        return $this->dishRepository->getDishes();
    }
}
