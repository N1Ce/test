<?php

namespace App\Application\UseCases\Clients\Dish\Contracts;

use App\Domain\Dish\Dish;

interface ShowDishCase
{
    public function execute(int $id): ?Dish;
}