<?php

namespace App\Application\UseCases\Clients\Dish;

use App\Application\UseCases\Clients\Dish\Contracts\ShowDishCase as ContractShowDishCase;
use App\Domain\Dish\Contracts\DishRepository;
use App\Domain\Dish\Dish;

final class ShowDishCase implements ContractShowDishCase
{
    private DishRepository $dishRepository;

    /**
     * ShowDishCase constructor.
     * @param DishRepository $dishRepository
     */
    public function __construct(DishRepository $dishRepository)
    {
        $this->dishRepository = $dishRepository;
    }

    /**
     * @param int $id
     * @return Dish|null
     */
    public function execute(int $id): ?Dish
    {
        return $this->dishRepository->getDishByIdAndImages($id);
    }
}
