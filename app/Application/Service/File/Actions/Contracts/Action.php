<?php

namespace App\Application\Service\File\Actions\Contracts;

use Illuminate\Http\UploadedFile;

interface Action
{
    public function save(UploadedFile $file): string;
}
