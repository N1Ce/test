<?php

namespace App\Application\Service\File\Actions;

use App\Application\Service\File\Actions\Contracts\Action;
use App\Infrastructure\Adapters\Storage\Contracts\Storage;
use Illuminate\Http\UploadedFile;

class DishAction implements Action
{
    private const DISH = 'dishes';

    private Storage $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }

    public function save(UploadedFile $file): string
    {
        $fileName = time().'.'.$file->getClientOriginalExtension();
        $path = self::DISH.'/'.$fileName;
        return $this->storage->save($path, $file);
    }
}
