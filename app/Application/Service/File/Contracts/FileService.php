<?php

namespace App\Application\Service\File\Contracts;

use Illuminate\Http\UploadedFile;

interface FileService
{
    public function buildAction(string $action): FileService;

    public function save(UploadedFile $file): string;
}
