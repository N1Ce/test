<?php

namespace App\Application\Service\File;

use App\Application\Service\File\Actions\Contracts\Action;
use App\Application\Service\File\Contracts\FileService as ContractFileService;
use App\Application\Service\File\Exception\FileException;
use App\Infrastructure\Adapters\Container\Contracts\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\UploadedFile;

class FileService implements ContractFileService
{
    private Action $action;

    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $action
     *
     * @return ContractFileService
     * @throws FileException
     */
    public function buildAction(string $action): ContractFileService
    {
        try {
            $this->action = $this->createEntity($action);
        } catch (BindingResolutionException $exception) {
            throw new FileException(sprintf(
                'Cannot build Action Strategy instance by given type - %s',
                $exception->getMessage()
            ));
        }

        return $this;
    }

    public function save(UploadedFile $file): string
    {
        return $this->action->save($file);
    }

    /**
     * @param string $action
     *
     * @return Action
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function createEntity(string $action): Action
    {
        return $this->container->make($action);
    }
}
